function noop() {}

function assign(tar, src) {
	for (var k in src) tar[k] = src[k];
	return tar;
}

function append(target, node) {
	target.appendChild(node);
}

function insert(target, node, anchor) {
	target.insertBefore(node, anchor);
}

function detachNode(node) {
	node.parentNode.removeChild(node);
}

function destroyEach(iterations, detach) {
	for (var i = 0; i < iterations.length; i += 1) {
		if (iterations[i]) iterations[i].d(detach);
	}
}

function createElement(name) {
	return document.createElement(name);
}

function createText(data) {
	return document.createTextNode(data);
}

function setData(text, data) {
	text.data = '' + data;
}

function setStyle(node, key, value) {
	node.style.setProperty(key, value);
}

function blankObject() {
	return Object.create(null);
}

function destroy(detach) {
	this.destroy = noop;
	this.fire('destroy');
	this.set = noop;

	this._fragment.d(detach !== false);
	this._fragment = null;
	this._state = {};
}

function _differs(a, b) {
	return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
}

function fire(eventName, data) {
	var handlers =
		eventName in this._handlers && this._handlers[eventName].slice();
	if (!handlers) return;

	for (var i = 0; i < handlers.length; i += 1) {
		var handler = handlers[i];

		if (!handler.__calling) {
			try {
				handler.__calling = true;
				handler.call(this, data);
			} finally {
				handler.__calling = false;
			}
		}
	}
}

function flush(component) {
	component._lock = true;
	callAll(component._beforecreate);
	callAll(component._oncreate);
	callAll(component._aftercreate);
	component._lock = false;
}

function get() {
	return this._state;
}

function init(component, options) {
	component._handlers = blankObject();
	component._slots = blankObject();
	component._bind = options._bind;
	component._staged = {};

	component.options = options;
	component.root = options.root || component;
	component.store = options.store || component.root.store;

	if (!options.root) {
		component._beforecreate = [];
		component._oncreate = [];
		component._aftercreate = [];
	}
}

function on(eventName, handler) {
	var handlers = this._handlers[eventName] || (this._handlers[eventName] = []);
	handlers.push(handler);

	return {
		cancel: function() {
			var index = handlers.indexOf(handler);
			if (~index) handlers.splice(index, 1);
		}
	};
}

function set(newState) {
	this._set(assign({}, newState));
	if (this.root._lock) return;
	flush(this.root);
}

function _set(newState) {
	var oldState = this._state,
		changed = {},
		dirty = false;

	newState = assign(this._staged, newState);
	this._staged = {};

	for (var key in newState) {
		if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
	}
	if (!dirty) return;

	this._state = assign(assign({}, oldState), newState);
	this._recompute(changed, this._state);
	if (this._bind) this._bind(changed, this._state);

	if (this._fragment) {
		this.fire("state", { changed: changed, current: this._state, previous: oldState });
		this._fragment.p(changed, this._state);
		this.fire("update", { changed: changed, current: this._state, previous: oldState });
	}
}

function _stage(newState) {
	assign(this._staged, newState);
}

function callAll(fns) {
	while (fns && fns.length) fns.shift()();
}

function _mount(target, anchor) {
	this._fragment[this._fragment.i ? 'i' : 'm'](target, anchor || null);
}

var proto = {
	destroy,
	get,
	fire,
	on,
	set,
	_recompute: noop,
	_set,
	_stage,
	_mount,
	_differs
};

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
  } : null;
}

function hexAndOpacityToRgb(hex, alpha) {
  const rgb = hexToRgb(hex);
  const oneMinusAlphaTimes255 = 255 * (1 - alpha);
  const r = rgb.r * alpha + oneMinusAlphaTimes255;
  const g = rgb.g * alpha + oneMinusAlphaTimes255;
  const b = rgb.b * alpha + oneMinusAlphaTimes255;
  return {r, g, b}
}

function rgbToHex(rgb) {
  return "#" + ((1 << 24) + (rgb.r << 16) + (rgb.g << 8) + rgb.b).toString(16).slice(1).split('.')[0]
}

function bestTextColor(rgb) {
  if ((rgb.r * 0.299 + rgb.g * 0.587 + rgb.b * 0.114) > 186) {
    return "#000000"
  } else {
    return "#ffffff"
  }
}

function getBackgroundAndTextColor(hex, alpha) {
  const rgb = hexAndOpacityToRgb(hex, alpha);
  const bgColor = rgbToHex(rgb);
  const textColor = bestTextColor(rgb);
  return {bgColor, textColor}
}

var colorUtils = {getBackgroundAndTextColor};
var colorUtils_1 = colorUtils.getBackgroundAndTextColor;

function getID() {
  const id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = crypto.getRandomValues(new Uint8Array(1))[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16)
  });
  return id
}

function div(i, j) {
  return Math.trunc(i / j)
}

function mod(i, j) {
  return i % j
}

var utils = {getID, div, mod};
var utils_1 = utils.getID;
var utils_2 = utils.div;
var utils_3 = utils.mod;

/* src/Grid.html generated by Svelte v2.13.2 */

function maxPracticeCount({disciplines}) {
  let maxPracticeCount = 0;
  for (let discipline of disciplines) {
    maxPracticeCount = Math.max(maxPracticeCount, discipline.practices.length);
  }
  return maxPracticeCount
}

function error({maxPracticeCount}) {
  if (maxPracticeCount > 8) {
    console.error("Max allowed practices is 8. You have " + maxPracticeCount + " in one (or more) discipline(s)");
    return true
  } else {
    return false
  }
}

function levelConfigAnnotated({levelConfig, baseColor}) {
  let levelConfigAnnotated = [];
  let baseColorCount = 0;
  for (let level of levelConfig) {
    if (! level.color)
      baseColorCount++;
  }
  let i = 0;
  for (let level of levelConfig) {
    if (! level.color) {
      level.color = baseColor;
      level.opacity = (baseColorCount - i - 1) / (baseColorCount - 1);
    } else {
      level.opacity = 1;
    }
    let colors = colorUtils_1(level.color, level.opacity);
    level.bgColor = colors.bgColor;
    level.textColor = colors.textColor;
    if (! level.description) {
      level.description = "";
    }
    levelConfigAnnotated.push(level);
    i++;
  }
  return levelConfigAnnotated
}

function disciplinesAnnotated({disciplines, levelConfigAnnotated, maxPracticeCount}) {
  let disciplinesAnnotated = disciplines;
  for (let discipline of disciplines) {
    let i = 0;
    const minCols = utils_2(maxPracticeCount, discipline.practices.length);
    let extraCols = utils_3(maxPracticeCount, discipline.practices.length);
    let textWidthArray = [];  // each row in the form {i, width}  width is in charCount for now but could be pixelWidth later
    for (let practice of discipline.practices) {
      practice.id = utils_1();
      let level = levelConfigAnnotated.find((level) => {
        return practice.assessment === level.label
      });
      practice.bgColor = level.bgColor;
      practice.textColor = level.textColor;
      textWidthArray.push({i, width: practice.label.length});
      practice.colspan = minCols;
      i++;
    }  // for practice...
    if (extraCols > 0) {
      textWidthArray.sort((a, b) => a.width - b.width);
      while (extraCols > 0) {
        let {i} = textWidthArray.pop();
        discipline.practices[i].colspan++;
        extraCols--;
      }
    }
  }  // for discipline...
  return disciplinesAnnotated
}

function data() {
  return {
    baseColor: "#2E8468",
  }
}
function add_css() {
	var style = createElement("style");
	style.id = 'svelte-17f3t0z-style';
	style.textContent = ".center.svelte-17f3t0z{text-align:center}";
	append(document.head, style);
}

function create_main_fragment(component, ctx) {
	var table, tr, th, text_1, th_1, text_4;

	var each_value = ctx.disciplines;

	var each_blocks = [];

	for (var i = 0; i < each_value.length; i += 1) {
		each_blocks[i] = create_each_block(component, get_each_context(ctx, each_value, i));
	}

	return {
		c() {
			table = createElement("table");
			tr = createElement("tr");
			th = createElement("th");
			th.textContent = "Disciplines";
			text_1 = createText("\n    ");
			th_1 = createElement("th");
			th_1.textContent = "Practices";
			text_4 = createText("\n  ");

			for (var i = 0; i < each_blocks.length; i += 1) {
				each_blocks[i].c();
			}
			th_1.colSpan = ctx.maxPracticeCount;
			th_1.className = "center svelte-17f3t0z";
		},

		m(target, anchor) {
			insert(target, table, anchor);
			append(table, tr);
			append(tr, th);
			append(tr, text_1);
			append(tr, th_1);
			append(table, text_4);

			for (var i = 0; i < each_blocks.length; i += 1) {
				each_blocks[i].m(table, null);
			}
		},

		p(changed, ctx) {
			if (changed.maxPracticeCount) {
				th_1.colSpan = ctx.maxPracticeCount;
			}

			if (changed.disciplines) {
				each_value = ctx.disciplines;

				for (var i = 0; i < each_value.length; i += 1) {
					const child_ctx = get_each_context(ctx, each_value, i);

					if (each_blocks[i]) {
						each_blocks[i].p(changed, child_ctx);
					} else {
						each_blocks[i] = create_each_block(component, child_ctx);
						each_blocks[i].c();
						each_blocks[i].m(table, null);
					}
				}

				for (; i < each_blocks.length; i += 1) {
					each_blocks[i].d(1);
				}
				each_blocks.length = each_value.length;
			}
		},

		d(detach) {
			if (detach) {
				detachNode(table);
			}

			destroyEach(each_blocks, detach);
		}
	};
}

// (6:2) {#each disciplines as discipline}
function create_each_block(component, ctx) {
	var tr, th, text_value = ctx.discipline.label, text, text_1;

	var each_value_1 = ctx.discipline.practices;

	var each_blocks = [];

	for (var i = 0; i < each_value_1.length; i += 1) {
		each_blocks[i] = create_each_block_1(component, get_each_context_1(ctx, each_value_1, i));
	}

	return {
		c() {
			tr = createElement("tr");
			th = createElement("th");
			text = createText(text_value);
			text_1 = createText("\n      ");

			for (var i = 0; i < each_blocks.length; i += 1) {
				each_blocks[i].c();
			}
		},

		m(target, anchor) {
			insert(target, tr, anchor);
			append(tr, th);
			append(th, text);
			append(tr, text_1);

			for (var i = 0; i < each_blocks.length; i += 1) {
				each_blocks[i].m(tr, null);
			}
		},

		p(changed, ctx) {
			if ((changed.disciplines) && text_value !== (text_value = ctx.discipline.label)) {
				setData(text, text_value);
			}

			if (changed.disciplines) {
				each_value_1 = ctx.discipline.practices;

				for (var i = 0; i < each_value_1.length; i += 1) {
					const child_ctx = get_each_context_1(ctx, each_value_1, i);

					if (each_blocks[i]) {
						each_blocks[i].p(changed, child_ctx);
					} else {
						each_blocks[i] = create_each_block_1(component, child_ctx);
						each_blocks[i].c();
						each_blocks[i].m(tr, null);
					}
				}

				for (; i < each_blocks.length; i += 1) {
					each_blocks[i].d(1);
				}
				each_blocks.length = each_value_1.length;
			}
		},

		d(detach) {
			if (detach) {
				detachNode(tr);
			}

			destroyEach(each_blocks, detach);
		}
	};
}

// (9:6) {#each discipline.practices as practice, i}
function create_each_block_1(component, ctx) {
	var td, text_value = ctx.practice.label, text, td_colspan_value;

	return {
		c() {
			td = createElement("td");
			text = createText(text_value);
			td.colSpan = td_colspan_value = ctx.practice.colspan;
			setStyle(td, "color", ctx.practice.textColor);
			setStyle(td, "background-color", ctx.practice.bgColor);
		},

		m(target, anchor) {
			insert(target, td, anchor);
			append(td, text);
		},

		p(changed, ctx) {
			if ((changed.disciplines) && text_value !== (text_value = ctx.practice.label)) {
				setData(text, text_value);
			}

			if ((changed.disciplines) && td_colspan_value !== (td_colspan_value = ctx.practice.colspan)) {
				td.colSpan = td_colspan_value;
			}

			if (changed.disciplines) {
				setStyle(td, "color", ctx.practice.textColor);
				setStyle(td, "background-color", ctx.practice.bgColor);
			}
		},

		d(detach) {
			if (detach) {
				detachNode(td);
			}
		}
	};
}

function get_each_context(ctx, list, i) {
	const child_ctx = Object.create(ctx);
	child_ctx.discipline = list[i];
	child_ctx.each_value = list;
	child_ctx.discipline_index = i;
	return child_ctx;
}

function get_each_context_1(ctx, list, i) {
	const child_ctx = Object.create(ctx);
	child_ctx.practice = list[i];
	child_ctx.each_value_1 = list;
	child_ctx.i = i;
	return child_ctx;
}

function Grid(options) {
	init(this, options);
	this._state = assign(data(), options.data);
	this._recompute({ disciplines: 1, maxPracticeCount: 1, levelConfig: 1, baseColor: 1, levelConfigAnnotated: 1 }, this._state);
	this._intro = true;

	if (!document.getElementById("svelte-17f3t0z-style")) add_css();

	this._fragment = create_main_fragment(this, this._state);

	if (options.target) {
		this._fragment.c();
		this._mount(options.target, options.anchor);
	}
}

assign(Grid.prototype, proto);

Grid.prototype._recompute = function _recompute(changed, state) {
	if (changed.disciplines) {
		if (this._differs(state.maxPracticeCount, (state.maxPracticeCount = maxPracticeCount(state)))) changed.maxPracticeCount = true;
	}

	if (changed.maxPracticeCount) {
		if (this._differs(state.error, (state.error = error(state)))) changed.error = true;
	}

	if (changed.levelConfig || changed.baseColor) {
		if (this._differs(state.levelConfigAnnotated, (state.levelConfigAnnotated = levelConfigAnnotated(state)))) changed.levelConfigAnnotated = true;
	}

	if (changed.disciplines || changed.levelConfigAnnotated || changed.maxPracticeCount) {
		if (this._differs(state.disciplinesAnnotated, (state.disciplinesAnnotated = disciplinesAnnotated(state)))) changed.disciplinesAnnotated = true;
	}
};

export default Grid;
//# sourceMappingURL=grid.mjs.map
