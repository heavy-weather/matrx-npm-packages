(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.Radar = factory());
}(this, (function () { 'use strict';

	function noop() {}

	function assign(tar, src) {
		for (var k in src) tar[k] = src[k];
		return tar;
	}

	function assignTrue(tar, src) {
		for (var k in src) tar[k] = 1;
		return tar;
	}

	function exclude(src, prop) {
		const tar = {};
		for (const k in src) k === prop || (tar[k] = src[k]);
		return tar;
	}

	function append(target, node) {
		target.appendChild(node);
	}

	function insert(target, node, anchor) {
		target.insertBefore(node, anchor);
	}

	function detachNode(node) {
		node.parentNode.removeChild(node);
	}

	function destroyEach(iterations, detach) {
		for (var i = 0; i < iterations.length; i += 1) {
			if (iterations[i]) iterations[i].d(detach);
		}
	}

	function createSvgElement(name) {
		return document.createElementNS('http://www.w3.org/2000/svg', name);
	}

	function createText(data) {
		return document.createTextNode(data);
	}

	function createComment() {
		return document.createComment('');
	}

	function addListener(node, event, handler, options) {
		node.addEventListener(event, handler, options);
	}

	function removeListener(node, event, handler, options) {
		node.removeEventListener(event, handler, options);
	}

	function setAttribute(node, attribute, value) {
		if (value == null) node.removeAttribute(attribute);
		else node.setAttribute(attribute, value);
	}

	function setXlinkAttribute(node, attribute, value) {
		node.setAttributeNS('http://www.w3.org/1999/xlink', attribute, value);
	}

	function setData(text, data) {
		text.data = '' + data;
	}

	function blankObject() {
		return Object.create(null);
	}

	function destroy(detach) {
		this.destroy = noop;
		this.fire('destroy');
		this.set = noop;

		this._fragment.d(detach !== false);
		this._fragment = null;
		this._state = {};
	}

	function _differs(a, b) {
		return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
	}

	function fire(eventName, data) {
		var handlers =
			eventName in this._handlers && this._handlers[eventName].slice();
		if (!handlers) return;

		for (var i = 0; i < handlers.length; i += 1) {
			var handler = handlers[i];

			if (!handler.__calling) {
				try {
					handler.__calling = true;
					handler.call(this, data);
				} finally {
					handler.__calling = false;
				}
			}
		}
	}

	function flush(component) {
		component._lock = true;
		callAll(component._beforecreate);
		callAll(component._oncreate);
		callAll(component._aftercreate);
		component._lock = false;
	}

	function get() {
		return this._state;
	}

	function init(component, options) {
		component._handlers = blankObject();
		component._slots = blankObject();
		component._bind = options._bind;
		component._staged = {};

		component.options = options;
		component.root = options.root || component;
		component.store = options.store || component.root.store;

		if (!options.root) {
			component._beforecreate = [];
			component._oncreate = [];
			component._aftercreate = [];
		}
	}

	function on(eventName, handler) {
		var handlers = this._handlers[eventName] || (this._handlers[eventName] = []);
		handlers.push(handler);

		return {
			cancel: function() {
				var index = handlers.indexOf(handler);
				if (~index) handlers.splice(index, 1);
			}
		};
	}

	function set(newState) {
		this._set(assign({}, newState));
		if (this.root._lock) return;
		flush(this.root);
	}

	function _set(newState) {
		var oldState = this._state,
			changed = {},
			dirty = false;

		newState = assign(this._staged, newState);
		this._staged = {};

		for (var key in newState) {
			if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
		}
		if (!dirty) return;

		this._state = assign(assign({}, oldState), newState);
		this._recompute(changed, this._state);
		if (this._bind) this._bind(changed, this._state);

		if (this._fragment) {
			this.fire("state", { changed: changed, current: this._state, previous: oldState });
			this._fragment.p(changed, this._state);
			this.fire("update", { changed: changed, current: this._state, previous: oldState });
		}
	}

	function _stage(newState) {
		assign(this._staged, newState);
	}

	function callAll(fns) {
		while (fns && fns.length) fns.shift()();
	}

	function _mount(target, anchor) {
		this._fragment[this._fragment.i ? 'i' : 'm'](target, anchor || null);
	}

	var proto = {
		destroy,
		get,
		fire,
		on,
		set,
		_recompute: noop,
		_set,
		_stage,
		_mount,
		_differs
	};

	function getID() {
	  const id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	    var r = crypto.getRandomValues(new Uint8Array(1))[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
	    return v.toString(16)
	  });
	  return id
	}

	function div(i, j) {
	  return Math.trunc(i / j)
	}

	function mod(i, j) {
	  return i % j
	}

	var utils = {getID, div, mod};
	var utils_1 = utils.getID;

	function hexToRgb(hex) {
	  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	  return result ? {
	      r: parseInt(result[1], 16),
	      g: parseInt(result[2], 16),
	      b: parseInt(result[3], 16)
	  } : null;
	}

	function hexAndOpacityToRgb(hex, alpha) {
	  const rgb = hexToRgb(hex);
	  const oneMinusAlphaTimes255 = 255 * (1 - alpha);
	  const r = rgb.r * alpha + oneMinusAlphaTimes255;
	  const g = rgb.g * alpha + oneMinusAlphaTimes255;
	  const b = rgb.b * alpha + oneMinusAlphaTimes255;
	  return {r, g, b}
	}

	function rgbToHex(rgb) {
	  return "#" + ((1 << 24) + (rgb.r << 16) + (rgb.g << 8) + rgb.b).toString(16).slice(1).split('.')[0]
	}

	function bestTextColor(rgb) {
	  if ((rgb.r * 0.299 + rgb.g * 0.587 + rgb.b * 0.114) > 186) {
	    return "#000000"
	  } else {
	    return "#ffffff"
	  }
	}

	function getBackgroundAndTextColor(hex, alpha) {
	  const rgb = hexAndOpacityToRgb(hex, alpha);
	  const bgColor = rgbToHex(rgb);
	  const textColor = bestTextColor(rgb);
	  return {bgColor, textColor}
	}

	var colorUtils = {getBackgroundAndTextColor};
	var colorUtils_1 = colorUtils.getBackgroundAndTextColor;

	var isObj = function (x) {
		var type = typeof x;
		return x !== null && (type === 'object' || type === 'function');
	};

	function getPathSegments(path) {
		const pathArr = path.split('.');
		const parts = [];

		for (let i = 0; i < pathArr.length; i++) {
			let p = pathArr[i];

			while (p[p.length - 1] === '\\' && pathArr[i + 1] !== undefined) {
				p = p.slice(0, -1) + '.';
				p += pathArr[++i];
			}

			parts.push(p);
		}

		return parts;
	}

	var dotProp = {
		get(obj, path, value) {
			if (!isObj(obj) || typeof path !== 'string') {
				return value === undefined ? obj : value;
			}

			const pathArr = getPathSegments(path);

			for (let i = 0; i < pathArr.length; i++) {
				if (!Object.prototype.propertyIsEnumerable.call(obj, pathArr[i])) {
					return value;
				}

				obj = obj[pathArr[i]];

				if (obj === undefined || obj === null) {
					// `obj` is either `undefined` or `null` so we want to stop the loop, and
					// if this is not the last bit of the path, and
					// if it did't return `undefined`
					// it would return `null` if `obj` is `null`
					// but we want `get({foo: null}, 'foo.bar')` to equal `undefined`, or the supplied value, not `null`
					if (i !== pathArr.length - 1) {
						return value;
					}

					break;
				}
			}

			return obj;
		},

		set(obj, path, value) {
			if (!isObj(obj) || typeof path !== 'string') {
				return obj;
			}

			const root = obj;
			const pathArr = getPathSegments(path);

			for (let i = 0; i < pathArr.length; i++) {
				const p = pathArr[i];

				if (!isObj(obj[p])) {
					obj[p] = {};
				}

				if (i === pathArr.length - 1) {
					obj[p] = value;
				}

				obj = obj[p];
			}

			return root;
		},

		delete(obj, path) {
			if (!isObj(obj) || typeof path !== 'string') {
				return;
			}

			const pathArr = getPathSegments(path);

			for (let i = 0; i < pathArr.length; i++) {
				const p = pathArr[i];

				if (i === pathArr.length - 1) {
					delete obj[p];
					return;
				}

				obj = obj[p];

				if (!isObj(obj)) {
					return;
				}
			}
		},

		has(obj, path) {
			if (!isObj(obj) || typeof path !== 'string') {
				return false;
			}

			const pathArr = getPathSegments(path);

			for (let i = 0; i < pathArr.length; i++) {
				if (isObj(obj)) {
					if (!(pathArr[i] in obj)) {
						return false;
					}

					obj = obj[pathArr[i]];
				} else {
					return false;
				}
			}

			return true;
		}
	};

	var arrify = function (val) {
		if (val === null || val === undefined) {
			return [];
		}

		return Array.isArray(val) ? val : [val];
	};

	const dotPropGet = dotProp.get;

	var sortOn = (arr, prop) => {
		if (!Array.isArray(arr)) {
			throw new TypeError('Expected an array');
		}

		return arr.slice().sort((a, b) => {
			let ret = 0;

			arrify(prop).some(el => {
				let x;
				let y;
				let desc;

				if (typeof el === 'function') {
					x = el(a);
					y = el(b);
				} else if (typeof el === 'string') {
					desc = el.charAt(0) === '-';
					el = desc ? el.slice(1) : el;
					x = dotPropGet(a, el);
					y = dotPropGet(b, el);
				} else {
					x = a;
					y = b;
				}

				if (x === y) {
					ret = 0;
					return false;
				}

				if (y !== 0 && !y) {
					ret = desc ? 1 : -1;
					return true;
				}

				if (x !== 0 && !x) {
					ret = desc ? -1 : 1;
					return true;
				}

				if (typeof x === 'string' && typeof y === 'string') {
					ret = desc ? y.localeCompare(x) : x.localeCompare(y);
					return ret !== 0;
				}

				if (desc) {
					ret = x < y ? 1 : -1;
				} else {
					ret = x < y ? -1 : 1;
				}

				return true;
			});

			return ret;
		});
	};

	function translateX(r, centerX, radians) {
	  return centerX + (r * Math.sin(radians))
	}

	function translateY(r, centerY, radians) {
	  return centerY - (r * Math.cos(radians))
	}

	function getArcEnds(r, centerX, centerY, startRadians, endRadians) {
	  let p1x = translateX(r, centerX, startRadians);
	  let p1y = translateY(r, centerY, startRadians);
	  let p2x = translateX(r, centerX, endRadians);
	  let p2y = translateY(r, centerY, endRadians);
	  return {p1x, p1y, p2x, p2y}
	}

	var arcTranslators = {translateX, translateY, getArcEnds};
	var arcTranslators_3 = arcTranslators.getArcEnds;

	/* src/Arc.html generated by Svelte v2.15.3 */



	function middleRadians({startRadians, endRadians}) {
		return (startRadians + endRadians) / 2;
	}

	function pointerEvents({fill}) {
		return (fill && !(fill==="none")) ? "all" : "none";
	}

	function innerArcEnds({innerRadius, centerX, centerY, startRadians, endRadians}) {
		return arcTranslators_3(innerRadius, centerX, centerY, startRadians, endRadians);
	}

	function outerArcEnds({outerRadius, centerX, centerY, startRadians, endRadians}) {
		return arcTranslators_3(outerRadius, centerX, centerY, startRadians, endRadians);
	}

	function arcSweep({endRadians, startRadians}) {
		return (endRadians - startRadians <= Math.PI) ? "0" : "1";
	}

	function pMRadius({innerRadius, outerRadius}) {
		return (innerRadius + outerRadius) / 2;
	}

	function labelArcEnds({pMRadius, centerX, centerY, startRadians, endRadians}) {
		return arcTranslators_3(pMRadius, centerX, centerY, startRadians, endRadians);
	}

	function data() {
	  return {
	    id: utils_1(),
	    arcPathID: utils_1(),
	    opacity: 1,
	    label: "",
	    fontSize: 1,
	  }
	}
	function create_main_fragment(component, ctx) {
		var g, defs, path, path_d_value, text1, textPath, text0, textPath_xlink_href_value;

		function select_block_type(ctx) {
			if ((ctx.middleRadians > ctx.Math.PI / 2) && (ctx.middleRadians < 3 * ctx.Math.PI / 2)) return create_if_block_1;
			return create_else_block;
		}

		var current_block_type = select_block_type(ctx);
		var if_block0 = current_block_type(component, ctx);

		var if_block1 = ((ctx.description)) && create_if_block(component, ctx);

		function mouseover_handler(event) {
			component.fire('mouseover', {event});
		}

		function mouseout_handler(event) {
			component.fire('mouseout', {event});
		}

		return {
			c() {
				g = createSvgElement("g");
				defs = createSvgElement("defs");
				if_block0.c();
				path = createSvgElement("path");
				if (if_block1) if_block1.c();
				text1 = createSvgElement("text");
				textPath = createSvgElement("textPath");
				text0 = createText(ctx.label);
				addListener(path, "mouseover", mouseover_handler);
				addListener(path, "mouseout", mouseout_handler);
				setAttribute(path, "pointer-events", ctx.pointerEvents);
				setAttribute(path, "fill", ctx.fill);
				setAttribute(path, "opacity", ctx.opacity);
				setAttribute(path, "stroke", ctx.stroke);
				setAttribute(path, "stroke-width", ctx.strokeWidth);
				setAttribute(path, "stroke-linecap", "square");
				setAttribute(path, "d", path_d_value = "\n      M " + ctx.innerArcEnds.p1x + " " + ctx.innerArcEnds.p1y + "\n      A " + ctx.innerRadius + " " + ctx.innerRadius + " 0 " + ctx.arcSweep + " 1 " + ctx.innerArcEnds.p2x + " " + ctx.innerArcEnds.p2y + "\n      L " + ctx.outerArcEnds.p2x + " " + ctx.outerArcEnds.p2y + "\n      A " + ctx.outerRadius + " " + ctx.outerRadius + " 1 " + ctx.arcSweep + " 0 " + ctx.outerArcEnds.p1x + " " + ctx.outerArcEnds.p1y + "\n      Z\n    ");
				setXlinkAttribute(textPath, "xlink:href", textPath_xlink_href_value = "#" + ctx.id);
				setAttribute(textPath, "dominant-baseline", "middle");
				setAttribute(textPath, "text-anchor", "middle");
				setAttribute(textPath, "startOffset", "50%");
				setAttribute(textPath, "fill", ctx.fontColor);
				setAttribute(text1, "pointer-events", "none");
				setAttribute(text1, "font-size", ctx.fontSize);
				setAttribute(text1, "font-family", "Helvetica Neue, Helvetica, Arial, sans-serif");
			},

			m(target, anchor) {
				insert(target, g, anchor);
				append(g, defs);
				if_block0.m(defs, null);
				append(g, path);
				if (if_block1) if_block1.m(path, null);
				append(g, text1);
				append(text1, textPath);
				append(textPath, text0);
			},

			p(changed, ctx) {
				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block0) {
					if_block0.p(changed, ctx);
				} else {
					if_block0.d(1);
					if_block0 = current_block_type(component, ctx);
					if_block0.c();
					if_block0.m(defs, null);
				}

				if ((ctx.description)) {
					if (if_block1) {
						if_block1.p(changed, ctx);
					} else {
						if_block1 = create_if_block(component, ctx);
						if_block1.c();
						if_block1.m(path, null);
					}
				} else if (if_block1) {
					if_block1.d(1);
					if_block1 = null;
				}

				if (changed.pointerEvents) {
					setAttribute(path, "pointer-events", ctx.pointerEvents);
				}

				if (changed.fill) {
					setAttribute(path, "fill", ctx.fill);
				}

				if (changed.opacity) {
					setAttribute(path, "opacity", ctx.opacity);
				}

				if (changed.stroke) {
					setAttribute(path, "stroke", ctx.stroke);
				}

				if (changed.strokeWidth) {
					setAttribute(path, "stroke-width", ctx.strokeWidth);
				}

				if ((changed.innerArcEnds || changed.innerRadius || changed.arcSweep || changed.outerArcEnds || changed.outerRadius) && path_d_value !== (path_d_value = "\n      M " + ctx.innerArcEnds.p1x + " " + ctx.innerArcEnds.p1y + "\n      A " + ctx.innerRadius + " " + ctx.innerRadius + " 0 " + ctx.arcSweep + " 1 " + ctx.innerArcEnds.p2x + " " + ctx.innerArcEnds.p2y + "\n      L " + ctx.outerArcEnds.p2x + " " + ctx.outerArcEnds.p2y + "\n      A " + ctx.outerRadius + " " + ctx.outerRadius + " 1 " + ctx.arcSweep + " 0 " + ctx.outerArcEnds.p1x + " " + ctx.outerArcEnds.p1y + "\n      Z\n    ")) {
					setAttribute(path, "d", path_d_value);
				}

				if (changed.label) {
					setData(text0, ctx.label);
				}

				if ((changed.id) && textPath_xlink_href_value !== (textPath_xlink_href_value = "#" + ctx.id)) {
					setXlinkAttribute(textPath, "xlink:href", textPath_xlink_href_value);
				}

				if (changed.fontColor) {
					setAttribute(textPath, "fill", ctx.fontColor);
				}

				if (changed.fontSize) {
					setAttribute(text1, "font-size", ctx.fontSize);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(g);
				}

				if_block0.d();
				if (if_block1) if_block1.d();
				removeListener(path, "mouseover", mouseover_handler);
				removeListener(path, "mouseout", mouseout_handler);
			}
		};
	}

	// (12:4) {:else}
	function create_else_block(component, ctx) {
		var path, path_d_value;

		return {
			c() {
				path = createSvgElement("path");
				setAttribute(path, "id", ctx.id);
				setAttribute(path, "d", path_d_value = "\n          M " + ctx.labelArcEnds.p1x + " " + ctx.labelArcEnds.p1y + "\n          A " + ctx.pMRadius + " " + ctx.pMRadius + " 0 " + ctx.arcSweep + " 1 " + ctx.labelArcEnds.p2x + " " + ctx.labelArcEnds.p2y + "\n        ");
			},

			m(target, anchor) {
				insert(target, path, anchor);
			},

			p(changed, ctx) {
				if (changed.id) {
					setAttribute(path, "id", ctx.id);
				}

				if ((changed.labelArcEnds || changed.pMRadius || changed.arcSweep) && path_d_value !== (path_d_value = "\n          M " + ctx.labelArcEnds.p1x + " " + ctx.labelArcEnds.p1y + "\n          A " + ctx.pMRadius + " " + ctx.pMRadius + " 0 " + ctx.arcSweep + " 1 " + ctx.labelArcEnds.p2x + " " + ctx.labelArcEnds.p2y + "\n        ")) {
					setAttribute(path, "d", path_d_value);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(path);
				}
			}
		};
	}

	// (4:4) {#if (middleRadians > Math.PI / 2) && (middleRadians < 3 * Math.PI / 2)}
	function create_if_block_1(component, ctx) {
		var path, path_d_value;

		return {
			c() {
				path = createSvgElement("path");
				setAttribute(path, "id", ctx.id);
				setAttribute(path, "d", path_d_value = "\n          M " + ctx.labelArcEnds.p2x + " " + ctx.labelArcEnds.p2y + "\n          A " + ctx.pMRadius + " " + ctx.pMRadius + " 1 " + ctx.arcSweep + " 0 " + ctx.labelArcEnds.p1x + " " + ctx.labelArcEnds.p1y + "\n        ");
			},

			m(target, anchor) {
				insert(target, path, anchor);
			},

			p(changed, ctx) {
				if (changed.id) {
					setAttribute(path, "id", ctx.id);
				}

				if ((changed.labelArcEnds || changed.pMRadius || changed.arcSweep) && path_d_value !== (path_d_value = "\n          M " + ctx.labelArcEnds.p2x + " " + ctx.labelArcEnds.p2y + "\n          A " + ctx.pMRadius + " " + ctx.pMRadius + " 1 " + ctx.arcSweep + " 0 " + ctx.labelArcEnds.p1x + " " + ctx.labelArcEnds.p1y + "\n        ")) {
					setAttribute(path, "d", path_d_value);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(path);
				}
			}
		};
	}

	// (38:4) {#if (description)}
	function create_if_block(component, ctx) {
		var title, text;

		return {
			c() {
				title = createSvgElement("title");
				text = createText(ctx.description);
			},

			m(target, anchor) {
				insert(target, title, anchor);
				append(title, text);
			},

			p(changed, ctx) {
				if (changed.description) {
					setData(text, ctx.description);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(title);
				}
			}
		};
	}

	function Arc(options) {
		init(this, options);
		this._state = assign(assign({ Math : Math }, data()), options.data);

		this._recompute({ startRadians: 1, endRadians: 1, fill: 1, innerRadius: 1, centerX: 1, centerY: 1, outerRadius: 1, pMRadius: 1 }, this._state);
		this._intro = true;

		this._fragment = create_main_fragment(this, this._state);

		if (options.target) {
			this._fragment.c();
			this._mount(options.target, options.anchor);
		}
	}

	assign(Arc.prototype, proto);

	Arc.prototype._recompute = function _recompute(changed, state) {
		if (changed.startRadians || changed.endRadians) {
			if (this._differs(state.middleRadians, (state.middleRadians = middleRadians(state)))) changed.middleRadians = true;
		}

		if (changed.fill) {
			if (this._differs(state.pointerEvents, (state.pointerEvents = pointerEvents(state)))) changed.pointerEvents = true;
		}

		if (changed.innerRadius || changed.centerX || changed.centerY || changed.startRadians || changed.endRadians) {
			if (this._differs(state.innerArcEnds, (state.innerArcEnds = innerArcEnds(state)))) changed.innerArcEnds = true;
		}

		if (changed.outerRadius || changed.centerX || changed.centerY || changed.startRadians || changed.endRadians) {
			if (this._differs(state.outerArcEnds, (state.outerArcEnds = outerArcEnds(state)))) changed.outerArcEnds = true;
		}

		if (changed.endRadians || changed.startRadians) {
			if (this._differs(state.arcSweep, (state.arcSweep = arcSweep(state)))) changed.arcSweep = true;
		}

		if (changed.innerRadius || changed.outerRadius) {
			if (this._differs(state.pMRadius, (state.pMRadius = pMRadius(state)))) changed.pMRadius = true;
		}

		if (changed.pMRadius || changed.centerX || changed.centerY || changed.startRadians || changed.endRadians) {
			if (this._differs(state.labelArcEnds, (state.labelArcEnds = labelArcEnds(state)))) changed.labelArcEnds = true;
		}
	};

	/* src/Goal.html generated by Svelte v2.15.3 */



	function middleRadians$1({startRadians, endRadians}) {
		return (startRadians + endRadians) / 2;
	}

	function goalLineArcEnds({goalAnnotated, centerX, centerY, startRadians, endRadians}) {
		return arcTranslators_3(goalAnnotated.radius, centerX, centerY, startRadians, endRadians);
	}

	function arcSweep$1({endRadians, startRadians}) {
		return (endRadians - startRadians <= Math.PI) ? "0" : "1";
	}

	function labelArcEnds$1({goalAnnotated, centerX, centerY, startRadians, endRadians, goalFontSize}) {
		return arcTranslators_3(goalAnnotated.radius - .75 * goalFontSize, centerX, centerY, startRadians, endRadians);
	}

	function data$1() {
	  return {
	    id: utils_1(),
	  }
	}
	function create_main_fragment$1(component, ctx) {
		var g, path, path_stroke_value, path_d_value, text1, textPath, text0_value = ctx.goalAnnotated.label, text0, textPath_xlink_href_value, textPath_fill_value;

		function select_block_type(ctx) {
			if ((ctx.middleRadians > ctx.Math.PI / 2) && (ctx.middleRadians < 3 * ctx.Math.PI / 2)) return create_if_block$1;
			return create_else_block$1;
		}

		var current_block_type = select_block_type(ctx);
		var if_block = current_block_type(component, ctx);

		return {
			c() {
				g = createSvgElement("g");
				path = createSvgElement("path");
				if_block.c();
				text1 = createSvgElement("text");
				textPath = createSvgElement("textPath");
				text0 = createText(text0_value);
				setAttribute(path, "fill", "none");
				setAttribute(path, "opacity", ctx.opacity);
				setAttribute(path, "stroke", path_stroke_value = ctx.goalAnnotated.goalColor);
				setAttribute(path, "stroke-width", .5);
				setAttribute(path, "stroke-linecap", "butt");
				setAttribute(path, "stroke-dasharray", "1, 0.5");
				setAttribute(path, "d", path_d_value = "\n      M " + ctx.goalLineArcEnds.p1x + " " + ctx.goalLineArcEnds.p1y + "\n      A " + ctx.goalAnnotated.radius + " " + ctx.goalAnnotated.radius + " 0 " + ctx.arcSweep + " 1 " + ctx.goalLineArcEnds.p2x + " " + ctx.goalLineArcEnds.p2y + "\n    ");
				setXlinkAttribute(textPath, "xlink:href", textPath_xlink_href_value = "#" + ctx.id);
				setAttribute(textPath, "dominant-baseline", "middle");
				setAttribute(textPath, "text-anchor", "middle");
				setAttribute(textPath, "startOffset", "50%");
				setAttribute(textPath, "fill", textPath_fill_value = ctx.goalAnnotated.goalColor);
				setAttribute(text1, "pointer-events", "none");
				setAttribute(text1, "font-size", ctx.goalFontSize);
				setAttribute(text1, "font-family", "-apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, Helvetica, Arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\"");
			},

			m(target, anchor) {
				insert(target, g, anchor);
				append(g, path);
				if_block.m(g, null);
				append(g, text1);
				append(text1, textPath);
				append(textPath, text0);
			},

			p(changed, ctx) {
				if (changed.opacity) {
					setAttribute(path, "opacity", ctx.opacity);
				}

				if ((changed.goalAnnotated) && path_stroke_value !== (path_stroke_value = ctx.goalAnnotated.goalColor)) {
					setAttribute(path, "stroke", path_stroke_value);
				}

				if ((changed.goalLineArcEnds || changed.goalAnnotated || changed.arcSweep) && path_d_value !== (path_d_value = "\n      M " + ctx.goalLineArcEnds.p1x + " " + ctx.goalLineArcEnds.p1y + "\n      A " + ctx.goalAnnotated.radius + " " + ctx.goalAnnotated.radius + " 0 " + ctx.arcSweep + " 1 " + ctx.goalLineArcEnds.p2x + " " + ctx.goalLineArcEnds.p2y + "\n    ")) {
					setAttribute(path, "d", path_d_value);
				}

				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if_block.d(1);
					if_block = current_block_type(component, ctx);
					if_block.c();
					if_block.m(g, text1);
				}

				if ((changed.goalAnnotated) && text0_value !== (text0_value = ctx.goalAnnotated.label)) {
					setData(text0, text0_value);
				}

				if ((changed.id) && textPath_xlink_href_value !== (textPath_xlink_href_value = "#" + ctx.id)) {
					setXlinkAttribute(textPath, "xlink:href", textPath_xlink_href_value);
				}

				if ((changed.goalAnnotated) && textPath_fill_value !== (textPath_fill_value = ctx.goalAnnotated.goalColor)) {
					setAttribute(textPath, "fill", textPath_fill_value);
				}

				if (changed.goalFontSize) {
					setAttribute(text1, "font-size", ctx.goalFontSize);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(g);
				}

				if_block.d();
			}
		};
	}

	// (23:2) {:else}
	function create_else_block$1(component, ctx) {
		var defs, path, path_d_value;

		return {
			c() {
				defs = createSvgElement("defs");
				path = createSvgElement("path");
				setAttribute(path, "id", ctx.id);
				setAttribute(path, "pointer-events", "none");
				setAttribute(path, "d", path_d_value = "\n          M " + ctx.labelArcEnds.p1x + " " + ctx.labelArcEnds.p1y + "\n          A " + ctx.goalAnnotated.radius + " " + ctx.goalAnnotated.radius + " 0 " + ctx.arcSweep + " 1 " + ctx.labelArcEnds.p2x + " " + ctx.labelArcEnds.p2y + "\n        ");
			},

			m(target, anchor) {
				insert(target, defs, anchor);
				append(defs, path);
			},

			p(changed, ctx) {
				if (changed.id) {
					setAttribute(path, "id", ctx.id);
				}

				if ((changed.labelArcEnds || changed.goalAnnotated || changed.arcSweep) && path_d_value !== (path_d_value = "\n          M " + ctx.labelArcEnds.p1x + " " + ctx.labelArcEnds.p1y + "\n          A " + ctx.goalAnnotated.radius + " " + ctx.goalAnnotated.radius + " 0 " + ctx.arcSweep + " 1 " + ctx.labelArcEnds.p2x + " " + ctx.labelArcEnds.p2y + "\n        ")) {
					setAttribute(path, "d", path_d_value);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(defs);
				}
			}
		};
	}

	// (12:2) {#if (middleRadians > Math.PI / 2) && (middleRadians < 3 * Math.PI / 2)}
	function create_if_block$1(component, ctx) {
		var defs, path, path_d_value;

		return {
			c() {
				defs = createSvgElement("defs");
				path = createSvgElement("path");
				setAttribute(path, "id", ctx.id);
				setAttribute(path, "pointer-events", "none");
				setAttribute(path, "d", path_d_value = "\n          M " + ctx.labelArcEnds.p2x + " " + ctx.labelArcEnds.p2y + "\n          A " + ctx.goalAnnotated.radius + " " + ctx.goalAnnotated.radius + " 1 " + ctx.arcSweep + " 0 " + ctx.labelArcEnds.p1x + " " + ctx.labelArcEnds.p1y + "\n        ");
			},

			m(target, anchor) {
				insert(target, defs, anchor);
				append(defs, path);
			},

			p(changed, ctx) {
				if (changed.id) {
					setAttribute(path, "id", ctx.id);
				}

				if ((changed.labelArcEnds || changed.goalAnnotated || changed.arcSweep) && path_d_value !== (path_d_value = "\n          M " + ctx.labelArcEnds.p2x + " " + ctx.labelArcEnds.p2y + "\n          A " + ctx.goalAnnotated.radius + " " + ctx.goalAnnotated.radius + " 1 " + ctx.arcSweep + " 0 " + ctx.labelArcEnds.p1x + " " + ctx.labelArcEnds.p1y + "\n        ")) {
					setAttribute(path, "d", path_d_value);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(defs);
				}
			}
		};
	}

	function Goal(options) {
		init(this, options);
		this._state = assign(assign({ Math : Math }, data$1()), options.data);

		this._recompute({ startRadians: 1, endRadians: 1, goalAnnotated: 1, centerX: 1, centerY: 1, goalFontSize: 1 }, this._state);
		this._intro = true;

		this._fragment = create_main_fragment$1(this, this._state);

		if (options.target) {
			this._fragment.c();
			this._mount(options.target, options.anchor);
		}
	}

	assign(Goal.prototype, proto);

	Goal.prototype._recompute = function _recompute(changed, state) {
		if (changed.startRadians || changed.endRadians) {
			if (this._differs(state.middleRadians, (state.middleRadians = middleRadians$1(state)))) changed.middleRadians = true;
		}

		if (changed.goalAnnotated || changed.centerX || changed.centerY || changed.startRadians || changed.endRadians) {
			if (this._differs(state.goalLineArcEnds, (state.goalLineArcEnds = goalLineArcEnds(state)))) changed.goalLineArcEnds = true;
		}

		if (changed.endRadians || changed.startRadians) {
			if (this._differs(state.arcSweep, (state.arcSweep = arcSweep$1(state)))) changed.arcSweep = true;
		}

		if (changed.goalAnnotated || changed.centerX || changed.centerY || changed.startRadians || changed.endRadians || changed.goalFontSize) {
			if (this._differs(state.labelArcEnds, (state.labelArcEnds = labelArcEnds$1(state)))) changed.labelArcEnds = true;
		}
	};

	/* src/Slice.html generated by Svelte v2.15.3 */

	function dataOuterRadius({outerRadius, labelBandHeight}) {
		return outerRadius - labelBandHeight;
	}

	function goalAnnotated({goal, goalColor, innerRadius, dataOuterRadius}) {
	  let goalAnnotated = goal;
	  if (goal) {
	    if (goal.percentage) {
	      goalAnnotated.radius = innerRadius + (dataOuterRadius - innerRadius) * goal.percentage;
	    } else {
	      goalAnnotated.radius = dataOuterRadius;
	    }
	    if (! goal.label) {
	      //default label
	      goalAnnotated.label = "";
	    }
	    if (! goal.goalColor) {
	      //default color for goal line and text
	      goalAnnotated.goalColor = goalColor;
	    }
	  }
	  return goalAnnotated
	}

	function data$2() {
	  return {
	  }
	}
	var methods = {
	  assessmentMouseover(event) {
	    this.fire('assessment-mouseover', {event});
	  },
	  assessmentMouseout(event) {
	    this.fire('assessment-mouseout', {event});
	  },
	};

	function get_each_context(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.assessment = list[i];
		return child_ctx;
	}

	function create_main_fragment$2(component, ctx) {
		var g, each_anchor;

		var each_value = ctx.assessmentData;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block(component, get_each_context(ctx, each_value, i));
		}

		var arc0_initial_data = {
		 	centerX: ctx.centerX,
		 	centerY: ctx.centerY,
		 	startRadians: ctx.startRadians,
		 	endRadians: ctx.endRadians,
		 	innerRadius: ctx.innerRadius,
		 	outerRadius: ctx.dataOuterRadius,
		 	strokeWidth: ctx.strokeWidth,
		 	stroke: ctx.stroke,
		 	fill: "none"
		 };
		var arc0 = new Arc({
			root: component.root,
			store: component.store,
			data: arc0_initial_data
		});

		var arc1_initial_data = {
		 	centerX: ctx.centerX,
		 	centerY: ctx.centerY,
		 	startRadians: ctx.startRadians,
		 	endRadians: ctx.endRadians,
		 	innerRadius: ctx.dataOuterRadius,
		 	outerRadius: ctx.outerRadius,
		 	fontColor: ctx.fontColor,
		 	fontSize: ctx.fontSize,
		 	strokeWidth: ctx.strokeWidth,
		 	stroke: ctx.stroke,
		 	fill: "#FFFFFF",
		 	label: ctx.label,
		 	description: ctx.description
		 };
		var arc1 = new Arc({
			root: component.root,
			store: component.store,
			data: arc1_initial_data
		});

		var if_block = (ctx.goal) && create_if_block$2(component, ctx);

		return {
			c() {
				g = createSvgElement("g");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				each_anchor = createComment();
				arc0._fragment.c();
				arc1._fragment.c();
				if (if_block) if_block.c();
			},

			m(target, anchor) {
				insert(target, g, anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(g, null);
				}

				append(g, each_anchor);
				arc0._mount(g, null);
				arc1._mount(g, null);
				if (if_block) if_block.m(g, null);
			},

			p(changed, ctx) {
				if (changed.centerX || changed.centerY || changed.startRadians || changed.endRadians || changed.assessmentData || changed.stroke || changed.strokeWidth) {
					each_value = ctx.assessmentData;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(g, each_anchor);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}

				var arc0_changes = {};
				if (changed.centerX) arc0_changes.centerX = ctx.centerX;
				if (changed.centerY) arc0_changes.centerY = ctx.centerY;
				if (changed.startRadians) arc0_changes.startRadians = ctx.startRadians;
				if (changed.endRadians) arc0_changes.endRadians = ctx.endRadians;
				if (changed.innerRadius) arc0_changes.innerRadius = ctx.innerRadius;
				if (changed.dataOuterRadius) arc0_changes.outerRadius = ctx.dataOuterRadius;
				if (changed.strokeWidth) arc0_changes.strokeWidth = ctx.strokeWidth;
				if (changed.stroke) arc0_changes.stroke = ctx.stroke;
				arc0._set(arc0_changes);

				var arc1_changes = {};
				if (changed.centerX) arc1_changes.centerX = ctx.centerX;
				if (changed.centerY) arc1_changes.centerY = ctx.centerY;
				if (changed.startRadians) arc1_changes.startRadians = ctx.startRadians;
				if (changed.endRadians) arc1_changes.endRadians = ctx.endRadians;
				if (changed.dataOuterRadius) arc1_changes.innerRadius = ctx.dataOuterRadius;
				if (changed.outerRadius) arc1_changes.outerRadius = ctx.outerRadius;
				if (changed.fontColor) arc1_changes.fontColor = ctx.fontColor;
				if (changed.fontSize) arc1_changes.fontSize = ctx.fontSize;
				if (changed.strokeWidth) arc1_changes.strokeWidth = ctx.strokeWidth;
				if (changed.stroke) arc1_changes.stroke = ctx.stroke;
				if (changed.label) arc1_changes.label = ctx.label;
				if (changed.description) arc1_changes.description = ctx.description;
				arc1._set(arc1_changes);

				if (ctx.goal) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block$2(component, ctx);
						if_block.c();
						if_block.m(g, null);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}
			},

			d(detach) {
				if (detach) {
					detachNode(g);
				}

				destroyEach(each_blocks, detach);

				arc0.destroy();
				arc1.destroy();
				if (if_block) if_block.d();
			}
		};
	}

	// (4:2) {#each assessmentData as assessment}
	function create_each_block(component, ctx) {

		var arc_initial_data = {
		 	centerX: ctx.centerX,
		 	centerY: ctx.centerY,
		 	startRadians: ctx.startRadians,
		 	endRadians: ctx.endRadians,
		 	innerRadius: ctx.assessment.innerRadius,
		 	outerRadius: ctx.assessment.outerRadius,
		 	fill: ctx.assessment.color,
		 	opacity: ctx.assessment.opacity,
		 	stroke: ctx.stroke,
		 	strokeWidth: ctx.strokeWidth
		 };
		var arc = new Arc({
			root: component.root,
			store: component.store,
			data: arc_initial_data
		});

		arc.on("mouseover", function(event) {
			component.fire('assessment-mouseover', {originalEvent: event, assessment: ctx.assessment});
		});
		arc.on("mouseout", function(event) {
			component.fire('assessment-mouseout', {originalEvent: event, assessment: ctx.assessment});
		});

		return {
			c() {
				arc._fragment.c();
			},

			m(target, anchor) {
				arc._mount(target, anchor);
			},

			p(changed, _ctx) {
				ctx = _ctx;
				var arc_changes = {};
				if (changed.centerX) arc_changes.centerX = ctx.centerX;
				if (changed.centerY) arc_changes.centerY = ctx.centerY;
				if (changed.startRadians) arc_changes.startRadians = ctx.startRadians;
				if (changed.endRadians) arc_changes.endRadians = ctx.endRadians;
				if (changed.assessmentData) arc_changes.innerRadius = ctx.assessment.innerRadius;
				if (changed.assessmentData) arc_changes.outerRadius = ctx.assessment.outerRadius;
				if (changed.assessmentData) arc_changes.fill = ctx.assessment.color;
				if (changed.assessmentData) arc_changes.opacity = ctx.assessment.opacity;
				if (changed.stroke) arc_changes.stroke = ctx.stroke;
				if (changed.strokeWidth) arc_changes.strokeWidth = ctx.strokeWidth;
				arc._set(arc_changes);
			},

			d(detach) {
				arc.destroy(detach);
			}
		};
	}

	// (29:2) {#if goal}
	function create_if_block$2(component, ctx) {

		var goal_initial_data = {
		 	centerX: ctx.centerX,
		 	centerY: ctx.centerY,
		 	startRadians: ctx.startRadians,
		 	endRadians: ctx.endRadians,
		 	goalAnnotated: ctx.goalAnnotated,
		 	goalFontSize: ctx.goalFontSize
		 };
		var goal = new Goal({
			root: component.root,
			store: component.store,
			data: goal_initial_data
		});

		return {
			c() {
				goal._fragment.c();
			},

			m(target, anchor) {
				goal._mount(target, anchor);
			},

			p(changed, ctx) {
				var goal_changes = {};
				if (changed.centerX) goal_changes.centerX = ctx.centerX;
				if (changed.centerY) goal_changes.centerY = ctx.centerY;
				if (changed.startRadians) goal_changes.startRadians = ctx.startRadians;
				if (changed.endRadians) goal_changes.endRadians = ctx.endRadians;
				if (changed.goalAnnotated) goal_changes.goalAnnotated = ctx.goalAnnotated;
				if (changed.goalFontSize) goal_changes.goalFontSize = ctx.goalFontSize;
				goal._set(goal_changes);
			},

			d(detach) {
				goal.destroy(detach);
			}
		};
	}

	function Slice(options) {
		init(this, options);
		this._state = assign(data$2(), options.data);

		this._recompute({ outerRadius: 1, labelBandHeight: 1, goal: 1, goalColor: 1, innerRadius: 1, dataOuterRadius: 1 }, this._state);
		this._intro = true;

		this._fragment = create_main_fragment$2(this, this._state);

		if (options.target) {
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(Slice.prototype, proto);
	assign(Slice.prototype, methods);

	Slice.prototype._recompute = function _recompute(changed, state) {
		if (changed.outerRadius || changed.labelBandHeight) {
			if (this._differs(state.dataOuterRadius, (state.dataOuterRadius = dataOuterRadius(state)))) changed.dataOuterRadius = true;
		}

		if (changed.goal || changed.goalColor || changed.innerRadius || changed.dataOuterRadius) {
			if (this._differs(state.goalAnnotated, (state.goalAnnotated = goalAnnotated(state)))) changed.goalAnnotated = true;
		}
	};

	/* src/Legend.html generated by Svelte v2.15.3 */

	function junk(s) {
	  // console.log(s.maxFontSize)
	  // console.log(s.scale)
	}

	function legendStartX({radarSize}) {
		return radarSize + 2;
	}

	function keySize({scale}) {
		return scale * 0.8;
	}

	function labelStartX({scale, legendStartX, keySize}) {
		return legendStartX + keySize + scale;
	}

	function textWidth({labelStartX, radarSize, legendWidth}) {
		return radarSize + legendWidth - 2 - labelStartX;
	}

	function descriptionStartX({labelStartX, labelDescriptionRatio, textWidth}) {
		return labelStartX + labelDescriptionRatio * textWidth;
	}

	function startY({levelConfigAnnotated, scale, radarSize}) {
		return (radarSize - (2 * scale * levelConfigAnnotated.length)) / 2;
	}

	function data$3() {
	  return {
	    dirty: true,
	    scale: 1,
	    margin: 1.0 / 2,
	    labelDescriptionRatio: 1.0 / 4,
	  }
	}
	function onupdate() {
	  let levelConfigAnnotated = this.get().levelConfigAnnotated;

	  let maxLabelWidth = 0;
	  let maxDescriptionWidth = 0;
	  for (var level of levelConfigAnnotated) {
	    level.labelWidth = document.getElementById(level.labelID).getBBox().width;
	    level.descriptionWidth = document.getElementById(level.descriptionID).getBBox().width;
	    if (level.labelWidth > maxLabelWidth) {
	      maxLabelWidth = level.labelWidth;
	    }
	    if (level.descriptionWidth > maxDescriptionWidth) {
	      maxDescriptionWidth = level.descriptionWidth;
	    }
	  }
	  let scale = this.get().textWidth / (maxLabelWidth + maxDescriptionWidth + 1);
	  if (scale > this.get().maxFontSize) {
	    scale = this.get().maxFontSize;
	  }

	  let labelDescriptionRatio = maxDescriptionWidth ? maxLabelWidth / maxDescriptionWidth : 0;

	  this.set({scale, labelDescriptionRatio});
	}
	function get_each_context$1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.level = list[i];
		child_ctx.i = i;
		return child_ctx;
	}

	function create_main_fragment$3(component, ctx) {
		var g;

		var each_value = ctx.levelConfigAnnotated;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block$1(component, get_each_context$1(ctx, each_value, i));
		}

		return {
			c() {
				g = createSvgElement("g");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
			},

			m(target, anchor) {
				insert(target, g, anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(g, null);
				}
			},

			p(changed, ctx) {
				if (changed.levelConfigAnnotated || changed.descriptionStartX || changed.startY || changed.scale || changed.legendFontColor || changed.labelStartX || changed.legendStartX || changed.keySize || changed.strokeColor || changed.strokeWidth) {
					each_value = ctx.levelConfigAnnotated;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$1(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block$1(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(g, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}
			},

			d(detach) {
				if (detach) {
					detachNode(g);
				}

				destroyEach(each_blocks, detach);
			}
		};
	}

	// (2:2) {#each levelConfigAnnotated as level, i}
	function create_each_block$1(component, ctx) {
		var rect, rect_x_value, rect_y_value, rect_fill_value, rect_fill_opacity_value, text1, text0_value = ctx.level.label, text0, text1_id_value, text1_y_value, text3, text2_value = ctx.level.description, text2, text3_id_value, text3_y_value;

		return {
			c() {
				rect = createSvgElement("rect");
				text1 = createSvgElement("text");
				text0 = createText(text0_value);
				text3 = createSvgElement("text");
				text2 = createText(text2_value);
				setAttribute(rect, "x", rect_x_value = ctx.legendStartX+0.25*ctx.scale);
				setAttribute(rect, "y", rect_y_value = ctx.startY+ctx.i*2*ctx.scale+0.25*ctx.scale);
				setAttribute(rect, "height", ctx.keySize);
				setAttribute(rect, "width", ctx.keySize);
				setAttribute(rect, "fill", rect_fill_value = ctx.level.color);
				setAttribute(rect, "fill-opacity", rect_fill_opacity_value = ctx.level.opacity);
				setAttribute(rect, "stroke", ctx.strokeColor);
				setAttribute(rect, "stroke-width", ctx.strokeWidth);
				setAttribute(text1, "id", text1_id_value = ctx.level.labelID);
				setAttribute(text1, "x", ctx.labelStartX);
				setAttribute(text1, "y", text1_y_value = ctx.startY+ctx.i*2*ctx.scale+ctx.scale);
				setAttribute(text1, "font-size", ctx.scale);
				setAttribute(text1, "text-anchor", "left");
				setAttribute(text1, "fill", ctx.legendFontColor);
				setAttribute(text1, "text-rendering", "geometricPrecision");
				setAttribute(text1, "font-family", "Helvetica Neue, Helvetica, Arial, sans-serif");
				setAttribute(text3, "id", text3_id_value = ctx.level.descriptionID);
				setAttribute(text3, "x", ctx.descriptionStartX);
				setAttribute(text3, "y", text3_y_value = ctx.startY+ctx.i*2*ctx.scale+ctx.scale);
				setAttribute(text3, "font-size", ctx.scale);
				setAttribute(text3, "text-anchor", "left");
				setAttribute(text3, "fill", ctx.legendFontColor);
				setAttribute(text3, "text-rendering", "geometricPrecision");
				setAttribute(text3, "font-family", "Helvetica Neue, Helvetica, Arial, sans-serif");
			},

			m(target, anchor) {
				insert(target, rect, anchor);
				insert(target, text1, anchor);
				append(text1, text0);
				insert(target, text3, anchor);
				append(text3, text2);
			},

			p(changed, ctx) {
				if ((changed.legendStartX || changed.scale) && rect_x_value !== (rect_x_value = ctx.legendStartX+0.25*ctx.scale)) {
					setAttribute(rect, "x", rect_x_value);
				}

				if ((changed.startY || changed.scale) && rect_y_value !== (rect_y_value = ctx.startY+ctx.i*2*ctx.scale+0.25*ctx.scale)) {
					setAttribute(rect, "y", rect_y_value);
				}

				if (changed.keySize) {
					setAttribute(rect, "height", ctx.keySize);
					setAttribute(rect, "width", ctx.keySize);
				}

				if ((changed.levelConfigAnnotated) && rect_fill_value !== (rect_fill_value = ctx.level.color)) {
					setAttribute(rect, "fill", rect_fill_value);
				}

				if ((changed.levelConfigAnnotated) && rect_fill_opacity_value !== (rect_fill_opacity_value = ctx.level.opacity)) {
					setAttribute(rect, "fill-opacity", rect_fill_opacity_value);
				}

				if (changed.strokeColor) {
					setAttribute(rect, "stroke", ctx.strokeColor);
				}

				if (changed.strokeWidth) {
					setAttribute(rect, "stroke-width", ctx.strokeWidth);
				}

				if ((changed.levelConfigAnnotated) && text0_value !== (text0_value = ctx.level.label)) {
					setData(text0, text0_value);
				}

				if ((changed.levelConfigAnnotated) && text1_id_value !== (text1_id_value = ctx.level.labelID)) {
					setAttribute(text1, "id", text1_id_value);
				}

				if (changed.labelStartX) {
					setAttribute(text1, "x", ctx.labelStartX);
				}

				if ((changed.startY || changed.scale) && text1_y_value !== (text1_y_value = ctx.startY+ctx.i*2*ctx.scale+ctx.scale)) {
					setAttribute(text1, "y", text1_y_value);
				}

				if (changed.scale) {
					setAttribute(text1, "font-size", ctx.scale);
				}

				if (changed.legendFontColor) {
					setAttribute(text1, "fill", ctx.legendFontColor);
				}

				if ((changed.levelConfigAnnotated) && text2_value !== (text2_value = ctx.level.description)) {
					setData(text2, text2_value);
				}

				if ((changed.levelConfigAnnotated) && text3_id_value !== (text3_id_value = ctx.level.descriptionID)) {
					setAttribute(text3, "id", text3_id_value);
				}

				if (changed.descriptionStartX) {
					setAttribute(text3, "x", ctx.descriptionStartX);
				}

				if ((changed.startY || changed.scale) && text3_y_value !== (text3_y_value = ctx.startY+ctx.i*2*ctx.scale+ctx.scale)) {
					setAttribute(text3, "y", text3_y_value);
				}

				if (changed.scale) {
					setAttribute(text3, "font-size", ctx.scale);
				}

				if (changed.legendFontColor) {
					setAttribute(text3, "fill", ctx.legendFontColor);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(rect);
					detachNode(text1);
					detachNode(text3);
				}
			}
		};
	}

	function Legend(options) {
		init(this, options);
		this._state = assign(data$3(), options.data);

		this._recompute({ radarSize: 1, scale: 1, legendStartX: 1, keySize: 1, labelStartX: 1, legendWidth: 1, labelDescriptionRatio: 1, textWidth: 1, levelConfigAnnotated: 1 }, this._state);
		this._intro = true;
		this._handlers.update = [onupdate];

		this._fragment = create_main_fragment$3(this, this._state);

		this.root._oncreate.push(() => {
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(Legend.prototype, proto);

	Legend.prototype._recompute = function _recompute(changed, state) {
		if (changed.radarSize) {
			if (this._differs(state.legendStartX, (state.legendStartX = legendStartX(state)))) changed.legendStartX = true;
		}

		if (changed.scale) {
			if (this._differs(state.keySize, (state.keySize = keySize(state)))) changed.keySize = true;
		}

		if (changed.scale || changed.legendStartX || changed.keySize) {
			if (this._differs(state.labelStartX, (state.labelStartX = labelStartX(state)))) changed.labelStartX = true;
		}

		if (changed.labelStartX || changed.radarSize || changed.legendWidth) {
			if (this._differs(state.textWidth, (state.textWidth = textWidth(state)))) changed.textWidth = true;
		}

		if (changed.labelStartX || changed.labelDescriptionRatio || changed.textWidth) {
			if (this._differs(state.descriptionStartX, (state.descriptionStartX = descriptionStartX(state)))) changed.descriptionStartX = true;
		}

		if (changed.levelConfigAnnotated || changed.scale || changed.radarSize) {
			if (this._differs(state.startY, (state.startY = startY(state)))) changed.startY = true;
		}

		if (this._differs(state.junk, (state.junk = junk(exclude(state, "junk"))))) changed.junk = true;
	};

	/* src/Radar.html generated by Svelte v2.15.3 */



	function viewBoxStartY({title, showTitle, titleHeight}) {
		return (title && showTitle) ? -1 * titleHeight : 0;
	}

	function viewBoxHeight({radarSize, title, showTitle, titleHeight}) {
		return (title && showTitle) ? radarSize + titleHeight : radarSize;
	}

	function centerXViewBox({radarSize, legendWidth}) {
		return (radarSize + legendWidth) / 2;
	}

	function centerX({radarSize}) {
		return radarSize / 2;
	}

	function centerY({radarSize}) {
		return radarSize / 2;
	}

	function outerRadius({radarSize}) {
		return radarSize / 2 - 0.5;
	}

	function disciplineBandHeight({radarSize}) {
		return 0.07 * radarSize;
	}

	function practiceBandHeight({radarSize}) {
		return 0.06 * radarSize;
	}

	function practiceCount({disciplines}) {
	  let practiceCount = 0;
	  for (let discipline of disciplines) {
	    for (let practice of discipline.practices) {
	      practiceCount++;
	    }
	  }
	  return practiceCount
	}

	function sliceWidth({practiceCount}) {
		return Math.PI * 2 / practiceCount;
	}

	function disciplineMaxWidth({outerRadius, disciplineBandHeight, sliceWidth}) {
		return 0.95 * sliceWidth * (outerRadius - disciplineBandHeight / 2);
	}

	function practiceMaxWidth({outerRadius, disciplineBandHeight, practiceBandHeight, sliceWidth}) {
		return 0.95 * sliceWidth * (outerRadius - disciplineBandHeight - practiceBandHeight / 2);
	}

	function goalMaxWidth({practiceMaxWidth}) {
		return 0.80 * practiceMaxWidth;
	}

	function disciplineMaxHeight({disciplineBandHeight}) {
		return 0.80 * disciplineBandHeight;
	}

	function practiceMaxHeight({practiceBandHeight}) {
		return 0.80 * practiceBandHeight;
	}

	function goalMaxHeight({practiceMaxHeight}) {
		return practiceMaxHeight;
	}

	function levelConfigAnnotated({levelConfig, baseColor, descriptionsInLegend}) {
	  let levelConfigAnnotated = [];
	  let baseColorCount = 0;
	  for (let level of levelConfig) {
	    if (! level.color)
	      baseColorCount++;
	  }
	  let i = 0;
	  for (let level of levelConfig) {
	    let rawColor = level.color || baseColor;
	    let alpha = 1;
	    if (! level.color) {
	      alpha = (baseColorCount - i - 1) / (baseColorCount - 1);
	    }
	    if (! level.description || ! descriptionsInLegend) {
	      level.description = "";
	    }
	    level.color = colorUtils_1(rawColor, alpha).bgColor;
	    level.labelID = utils_1();
	    level.descriptionID = utils_1();
	    level.levelIndex = i;
	    levelConfigAnnotated.push(level);
	    i++;
	  }
	  return levelConfigAnnotated
	}

	function lookupLevelByID({levelConfigAnnotated}) {
	  let lookup = {};
	  for (let level of levelConfigAnnotated) {
	    lookup[level["_entityID"]] = level;
	  }
	  return lookup
	}

	function lookupTeamByID({teams}) {
	  let lookup = {};
	  let i = 0;
	  for (let team of teams) {
	    team.teamIndex = i;
	    lookup[team["_entityID"]] = team;
	    i++;
	  }
	  return lookup
	}

	function disciplinesAnnotated({highlightedTeamID, levelConfigAnnotated, lookupLevelByID, lookupTeamByID, disciplines, assessmentData, sliceWidth, innerRadius, outerRadius, practiceBandHeight, disciplineBandHeight}) {
	  let disciplinesAnnotated = disciplines;
	  let currentAngle = 0;
	  let lookupByPracticeID = {};
	  for (let discipline of disciplines) {
	    discipline.id = utils_1();
	    discipline.startRadians = currentAngle;
	    for (let practice of discipline.practices) {
	      practice.assessmentData = [];
	      lookupByPracticeID[practice["_entityID"]] = practice;
	      practice.id = utils_1();
	      practice.startRadians = currentAngle;
	      if (practice.goal) {
	        practice.goal.id = utils_1();
	      }
	      currentAngle += sliceWidth;
	      practice.endRadians = currentAngle;
	    }
	    discipline.endRadians = currentAngle;
	  }

	  // Populate assessmentData inside each practice
	  for (let row of assessmentData) {
	    let level = lookupLevelByID[row.levelID];
	    row.levelIndex = level.levelIndex;
	    if (! highlightedTeamID || row.teamID === highlightedTeamID) {
	      row.color = level.color;
	    } else {
	      row.color = "#666666";
	    }
	    row.opacity = 1;
	    let team = lookupTeamByID[row.teamID];
	    row.teamIndex = team.teamIndex;
	    let practice = lookupByPracticeID[row.practiceID];
	    if (practice) {
	      practice.assessmentData.push(row);
	    }
	  }
	  // Sort the assessmentData levelIndex and then teamIndex. Calculate inner and outer radius for each assessmentData row.
	  for (let practiceID in lookupByPracticeID) {
	    let practice = lookupByPracticeID[practiceID];
	    practice.assessmentData = sortOn(practice.assessmentData, ['levelIndex', 'teamIndex']);
	    let r = innerRadius;
	    let arcHeight = (outerRadius - practiceBandHeight - disciplineBandHeight - innerRadius) / practice.assessmentData.length;
	    for (let row of practice.assessmentData) {
	      row.innerRadius = r;
	      row.outerRadius = r + arcHeight;
	      r += arcHeight;
	    }

	    // Set percentage for goal excluding level so marked
	    if (! practice.goal.percentage) {
	      let total = 0;
	      let totalExcludedFromGoal = 0;
	      for (let assessment of practice.assessmentData) {
	        total++;
	        if (lookupLevelByID[assessment.levelID].excludeFromGoal) {
	          totalExcludedFromGoal++;
	        }
	      }
	      practice.goal.percentage = 1.0 - totalExcludedFromGoal / total;
	    }

	  }

	  return disciplinesAnnotated
	}

	function goalColor({goalColor, levelConfigAnnotated}) {
	  if (! goalColor) {
	    return levelConfigAnnotated[0].color
	  } else {
	    return goalColor
	  }
	}

	function data$4() {
	  return {
	    _logoBackgroundColor: "#FFFFFF",
	    _logoMainColor: "#808080",
	    title: "MatrX DevSecOps Radar",
	    subTitle: null,
	    showTitle: true,
	    showSubTitle: true,
	    titleHeight: 10,
	    showLogo: true,
	    innerRadius: 10,
	    radarSize: 100, // height and width
	    legendWidth: 50,
	    descriptionsInLegend: true,
	    practiceStroke: "#999999",
	    disciplineStroke: "#555555",
	    strokeWidth: 0.15,
	    baseColor: "#2E8468",
	    fontColor: "#22644E",
	    practiceFontSize: 1,
	    disciplineFontSize: 1,
	    goalFontSize: 1,
	    highlightedTeamID: null,
	    legendFontColor: "#006de0",
	    diciplineFontColor: "#184738",
	    disciplines: [
	      {
	        discipline: "No data",
	        practices: [
	          {practice: "Instructions: https://github.com/morganmaccherone/matrx-radar/blob/master/README.md", levels: [
	            {portion: 1},
	          ]},
	        ]
	      },
	      {
	        discipline: "No data",
	        practices: [
	          {practice: "Editable example: TBD", levels: [
	            {portion: 1},
	          ]},
	        ]
	      },
	    ]
	  }
	}
	var methods$1 = {
	  assessmentMouseover(e) {
	    let {lookupTeamByID} = this.get();
	    let subTitle = lookupTeamByID[e.assessment.teamID].label;
	    this.set({
	      highlightedTeamID: e.assessment.teamID,
	      subTitle: subTitle,
	    });
	    const event = new CustomEvent('assessment-mouseover', {
	      detail: e.assessment,
	    	bubbles: true,
	    	cancelable: true,
	    	composed: true, // makes the event jump shadow DOM boundary
	    });
	    let source = e.originalEvent.event.target;
	    source.dispatchEvent(event);
	  },
	  assessmentMouseout(e) {
	    this.set({highlightedTeamID: null, subTitle: null});
	    const event = new CustomEvent('assessment-mouseout', {
	      detail: e.assessment,
	      bubbles: true,
	      cancelable: true,
	      composed: true, // makes the event jump shadow DOM boundary
	    });
	    let source = e.originalEvent.event.target;
	    source.dispatchEvent(event);
	  },
	};

	function oncreate() {
	  let disciplinesAnnotated = this.get().disciplinesAnnotated;

	  let maxPracticeWidth = 0;
	  let maxPracticeHeight = 0;
	  let maxDisciplineWidth = 0;
	  let maxDisciplineHeight = 0;
	  let maxGoalWidth = 0;
	  let maxGoalHeight = 0;

	  for (let discipline of disciplinesAnnotated) {
	    maxDisciplineWidth = Math.max(maxDisciplineWidth, document.getElementById(discipline.id).getBBox().width);
	    maxDisciplineHeight = Math.max(maxDisciplineHeight, document.getElementById(discipline.id).getBBox().height);
	    for (let practice of discipline.practices) {
	      maxPracticeWidth = Math.max(maxPracticeWidth, document.getElementById(practice.id).getBBox().width);
	      maxPracticeHeight = Math.max(maxPracticeHeight, document.getElementById(practice.id).getBBox().height);
	      if (practice.goal) {
	        maxGoalWidth = Math.max(maxGoalWidth, document.getElementById(practice.goal.id).getBBox().width);
	        maxGoalHeight = Math.max(maxGoalHeight, document.getElementById(practice.goal.id).getBBox().height);
	      }
	    }
	  }

	  let practiceFontSize = Math.min(this.get().practiceMaxWidth / maxPracticeWidth, this.get().practiceMaxHeight / maxPracticeHeight);
	  let disciplineFontSize = Math.min(this.get().disciplineMaxWidth / maxDisciplineWidth, this.get().disciplineMaxHeight / maxDisciplineHeight);
	  let goalFontSize = Math.min(this.get().goalMaxWidth / maxGoalWidth, this.get().goalMaxHeight / maxGoalHeight);

	  if (practiceFontSize > disciplineFontSize) {
	    practiceFontSize = disciplineFontSize;
	  }

	  if (goalFontSize > practiceFontSize) {
	    goalFontSize = practiceFontSize;
	  }

	  this.set({disciplineFontSize, practiceFontSize, goalFontSize});

	}
	function get_each_context_1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.practice = list[i];
		return child_ctx;
	}

	function get_each_context$2(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.discipline = list[i];
		return child_ctx;
	}

	function create_main_fragment$4(component, ctx) {
		var svg, if_block0_anchor, if_block1_anchor, each_anchor, svg_viewBox_value;

		var if_block0 = (ctx.showLogo) && create_if_block_3(component, ctx);

		var if_block1 = (ctx.title && ctx.showTitle) && create_if_block_1$1(component, ctx);

		var each_value = ctx.disciplinesAnnotated;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block$2(component, get_each_context$2(ctx, each_value, i));
		}

		var legend_initial_data = {
		 	levelConfigAnnotated: ctx.levelConfigAnnotated,
		 	maxFontSize: ctx.disciplineFontSize,
		 	strokeWidth: ctx.strokeWidth,
		 	strokeColor: ctx.practiceStroke,
		 	legendFontColor: ctx.legendFontColor,
		 	legendWidth: ctx.legendWidth,
		 	radarSize: ctx.radarSize
		 };
		var legend = new Legend({
			root: component.root,
			store: component.store,
			data: legend_initial_data
		});

		return {
			c() {
				svg = createSvgElement("svg");
				if (if_block0) if_block0.c();
				if_block0_anchor = createComment();
				if (if_block1) if_block1.c();
				if_block1_anchor = createComment();

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				each_anchor = createComment();
				legend._fragment.c();
				setAttribute(svg, "version", "1.1");
				setAttribute(svg, "viewBox", svg_viewBox_value = "0 " + ctx.viewBoxStartY + " " + (ctx.radarSize+ctx.legendWidth) + " " + ctx.viewBoxHeight);
				setAttribute(svg, "preserveAspectRatio", "xMinYMin meet");
			},

			m(target, anchor) {
				insert(target, svg, anchor);
				if (if_block0) if_block0.m(svg, null);
				append(svg, if_block0_anchor);
				if (if_block1) if_block1.m(svg, null);
				append(svg, if_block1_anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(svg, null);
				}

				append(svg, each_anchor);
				legend._mount(svg, null);
			},

			p(changed, ctx) {
				if (ctx.showLogo) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_3(component, ctx);
						if_block0.c();
						if_block0.m(svg, if_block0_anchor);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if (ctx.title && ctx.showTitle) {
					if (if_block1) {
						if_block1.p(changed, ctx);
					} else {
						if_block1 = create_if_block_1$1(component, ctx);
						if_block1.c();
						if_block1.m(svg, if_block1_anchor);
					}
				} else if (if_block1) {
					if_block1.d(1);
					if_block1 = null;
				}

				if (changed.centerX || changed.centerY || changed.disciplinesAnnotated || changed.outerRadius || changed.innerRadius || changed.strokeWidth || changed.disciplineStroke || changed.disciplineBandHeight || changed.diciplineFontColor || changed.disciplineFontSize || changed.goalFontSize || changed.goalColor || changed.practiceBandHeight || changed.practiceStroke || changed.fontColor || changed.practiceFontSize || changed.levelConfigAnnotated) {
					each_value = ctx.disciplinesAnnotated;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$2(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block$2(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(svg, each_anchor);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}

				var legend_changes = {};
				if (changed.levelConfigAnnotated) legend_changes.levelConfigAnnotated = ctx.levelConfigAnnotated;
				if (changed.disciplineFontSize) legend_changes.maxFontSize = ctx.disciplineFontSize;
				if (changed.strokeWidth) legend_changes.strokeWidth = ctx.strokeWidth;
				if (changed.practiceStroke) legend_changes.strokeColor = ctx.practiceStroke;
				if (changed.legendFontColor) legend_changes.legendFontColor = ctx.legendFontColor;
				if (changed.legendWidth) legend_changes.legendWidth = ctx.legendWidth;
				if (changed.radarSize) legend_changes.radarSize = ctx.radarSize;
				legend._set(legend_changes);

				if ((changed.viewBoxStartY || changed.radarSize || changed.legendWidth || changed.viewBoxHeight) && svg_viewBox_value !== (svg_viewBox_value = "0 " + ctx.viewBoxStartY + " " + (ctx.radarSize+ctx.legendWidth) + " " + ctx.viewBoxHeight)) {
					setAttribute(svg, "viewBox", svg_viewBox_value);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(svg);
				}

				if (if_block0) if_block0.d();
				if (if_block1) if_block1.d();

				destroyEach(each_blocks, detach);

				legend.destroy();
			}
		};
	}

	// (4:2) {#if showLogo}
	function create_if_block_3(component, ctx) {
		var circle, svg1, svg0, g, path0, path1, path2, path3, path4, path5, path6, path7, path8, path9, path10, path11, svg1_x_value, svg1_y_value, svg1_width_value, svg1_height_value, svg1_viewBox_value;

		return {
			c() {
				circle = createSvgElement("circle");
				svg1 = createSvgElement("svg");
				svg0 = createSvgElement("svg");
				g = createSvgElement("g");
				path0 = createSvgElement("path");
				path1 = createSvgElement("path");
				path2 = createSvgElement("path");
				path3 = createSvgElement("path");
				path4 = createSvgElement("path");
				path5 = createSvgElement("path");
				path6 = createSvgElement("path");
				path7 = createSvgElement("path");
				path8 = createSvgElement("path");
				path9 = createSvgElement("path");
				path10 = createSvgElement("path");
				path11 = createSvgElement("path");
				setAttribute(circle, "cx", ctx.centerX);
				setAttribute(circle, "cy", ctx.centerY);
				setAttribute(circle, "r", ctx.innerRadius);
				setAttribute(circle, "fill", ctx._logoBackgroundColor);
				setAttribute(path0, "d", "M156,291 L156,244 L166,244 L182,266 L197,244 L207,244 L207,292 L197,292 L197,261 L185,277 L178,277 L166,261 L166,292 L156,292 z");
				setAttribute(path0, "fill", ctx._logoMainColor);
				setAttribute(path1, "d", "M253.871,270 L246.742,270 L250.307,262.094 L253.871,254.188 L257.435,262.094 L261,270 z M257,244 L250,244 L230,292 L238,292 L244,276 L263,276 L270,292 L278,292 z");
				setAttribute(path1, "fill", ctx._logoMainColor);
				setAttribute(path2, "d", "M363,265 L363,250 L376,250 C376,250 382,251 382,258 C382,265 377,265 377,265 z M379,270 C379,270 389,270 389,258 C389,246 378,244 378,244 L356,244 L356,292 L363,292 L363,272 L372,272 L386,292 L395,292 z");
				setAttribute(path2, "fill", ctx._logoMainColor);
				setAttribute(path3, "d", "M311,292 L318,292 L318,250 L331,250 L331,244 L298,244 L298,250 L311,250 z");
				setAttribute(path3, "fill", ctx._logoMainColor);
				setAttribute(path4, "d", "M420,244 L432,244 L440,257 L442,257 L450,244 L462,244 L447,265 L447,267 L464,292 L453,292 L442,275 L440,275 L429,292 L418,292 L435,267 L435,265 z");
				setAttribute(path4, "fill", "#298567");
				setAttribute(path5, "d", "M225.215,352.083 L207.7,368.998 L190.785,351.482 L208.3,334.567 z");
				setAttribute(path5, "fill", "#B8FFE6");
				setAttribute(path6, "d", "M402.715,352.083 L385.2,368.998 L368.285,351.482 L385.8,334.567 z");
				setAttribute(path6, "fill", "#1F664F");
				setAttribute(path7, "d", "M260.715,352.083 L243.2,368.998 L226.285,351.482 L243.8,334.567 z");
				setAttribute(path7, "fill", "#86E4C2");
				setAttribute(path8, "d", "M296.215,352.083 L278.7,368.998 L261.785,351.482 L279.3,334.567 z");
				setAttribute(path8, "fill", "#3DCC9E");
				setAttribute(path9, "d", "M331.715,352.083 L314.2,368.998 L297.285,351.482 L314.8,334.567 z");
				setAttribute(path9, "fill", "#31A27D");
				setAttribute(path10, "d", "M438.215,352.083 L420.7,368.998 L403.785,351.482 L421.3,334.567 z");
				setAttribute(path10, "fill", "#154738");
				setAttribute(path11, "d", "M367.215,352.083 L349.7,368.998 L332.785,351.482 L350.3,334.567 z");
				setAttribute(path11, "fill", "#298567");
				setAttribute(g, "id", "Layer_1");
				setAttribute(g, "transform", "translate(-156, -244)");
				setAttribute(svg0, "version", "1.1");
				setAttribute(svg0, "xmlns", "http://www.w3.org/2000/svg");
				setAttribute(svg0, "viewBox", "-50, -50, 398, 215");
				setAttribute(svg1, "xmlns", "http://www.w3.org/2000/svg");
				setAttribute(svg1, "x", svg1_x_value = ctx.centerX-ctx.innerRadius);
				setAttribute(svg1, "y", svg1_y_value = ctx.centerY-ctx.innerRadius);
				setAttribute(svg1, "width", svg1_width_value = ctx.innerRadius*2);
				setAttribute(svg1, "height", svg1_height_value = ctx.innerRadius*2);
				setAttribute(svg1, "viewBox", svg1_viewBox_value = "0 0 " + ctx.innerRadius*2 + " " + ctx.innerRadius*2);
			},

			m(target, anchor) {
				insert(target, circle, anchor);
				insert(target, svg1, anchor);
				append(svg1, svg0);
				append(svg0, g);
				append(g, path0);
				append(g, path1);
				append(g, path2);
				append(g, path3);
				append(g, path4);
				append(g, path5);
				append(g, path6);
				append(g, path7);
				append(g, path8);
				append(g, path9);
				append(g, path10);
				append(g, path11);
			},

			p(changed, ctx) {
				if (changed.centerX) {
					setAttribute(circle, "cx", ctx.centerX);
				}

				if (changed.centerY) {
					setAttribute(circle, "cy", ctx.centerY);
				}

				if (changed.innerRadius) {
					setAttribute(circle, "r", ctx.innerRadius);
				}

				if (changed._logoBackgroundColor) {
					setAttribute(circle, "fill", ctx._logoBackgroundColor);
				}

				if (changed._logoMainColor) {
					setAttribute(path0, "fill", ctx._logoMainColor);
					setAttribute(path1, "fill", ctx._logoMainColor);
					setAttribute(path2, "fill", ctx._logoMainColor);
					setAttribute(path3, "fill", ctx._logoMainColor);
				}

				if ((changed.centerX || changed.innerRadius) && svg1_x_value !== (svg1_x_value = ctx.centerX-ctx.innerRadius)) {
					setAttribute(svg1, "x", svg1_x_value);
				}

				if ((changed.centerY || changed.innerRadius) && svg1_y_value !== (svg1_y_value = ctx.centerY-ctx.innerRadius)) {
					setAttribute(svg1, "y", svg1_y_value);
				}

				if ((changed.innerRadius) && svg1_width_value !== (svg1_width_value = ctx.innerRadius*2)) {
					setAttribute(svg1, "width", svg1_width_value);
				}

				if ((changed.innerRadius) && svg1_height_value !== (svg1_height_value = ctx.innerRadius*2)) {
					setAttribute(svg1, "height", svg1_height_value);
				}

				if ((changed.innerRadius) && svg1_viewBox_value !== (svg1_viewBox_value = "0 0 " + ctx.innerRadius*2 + " " + ctx.innerRadius*2)) {
					setAttribute(svg1, "viewBox", svg1_viewBox_value);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(circle);
					detachNode(svg1);
				}
			}
		};
	}

	// (26:2) {#if title && showTitle}
	function create_if_block_1$1(component, ctx) {
		var text1, text0, text1_y_value, if_block_anchor;

		var if_block = (ctx.subTitle && ctx.showSubTitle) && create_if_block_2(component, ctx);

		return {
			c() {
				text1 = createSvgElement("text");
				text0 = createText(ctx.title);
				if (if_block) if_block.c();
				if_block_anchor = createComment();
				setAttribute(text1, "text-anchor", "middle");
				setAttribute(text1, "x", ctx.centerXViewBox);
				setAttribute(text1, "y", text1_y_value = -2*ctx.titleHeight/4);
				setAttribute(text1, "font-size", "5");
				setAttribute(text1, "font-family", "Helvetica Neue, Helvetica, Arial, sans-serif");
			},

			m(target, anchor) {
				insert(target, text1, anchor);
				append(text1, text0);
				if (if_block) if_block.m(target, anchor);
				insert(target, if_block_anchor, anchor);
			},

			p(changed, ctx) {
				if (changed.title) {
					setData(text0, ctx.title);
				}

				if (changed.centerXViewBox) {
					setAttribute(text1, "x", ctx.centerXViewBox);
				}

				if ((changed.titleHeight) && text1_y_value !== (text1_y_value = -2*ctx.titleHeight/4)) {
					setAttribute(text1, "y", text1_y_value);
				}

				if (ctx.subTitle && ctx.showSubTitle) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block_2(component, ctx);
						if_block.c();
						if_block.m(if_block_anchor.parentNode, if_block_anchor);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}
			},

			d(detach) {
				if (detach) {
					detachNode(text1);
				}

				if (if_block) if_block.d(detach);
				if (detach) {
					detachNode(if_block_anchor);
				}
			}
		};
	}

	// (30:4) {#if subTitle && showSubTitle}
	function create_if_block_2(component, ctx) {
		var text1, text0, text1_y_value;

		return {
			c() {
				text1 = createSvgElement("text");
				text0 = createText(ctx.subTitle);
				setAttribute(text1, "text-anchor", "middle");
				setAttribute(text1, "x", ctx.centerXViewBox);
				setAttribute(text1, "y", text1_y_value = -1*ctx.titleHeight/8);
				setAttribute(text1, "font-size", "3");
				setAttribute(text1, "font-family", "Helvetica Neue, Helvetica, Arial, sans-serif");
			},

			m(target, anchor) {
				insert(target, text1, anchor);
				append(text1, text0);
			},

			p(changed, ctx) {
				if (changed.subTitle) {
					setData(text0, ctx.subTitle);
				}

				if (changed.centerXViewBox) {
					setAttribute(text1, "x", ctx.centerXViewBox);
				}

				if ((changed.titleHeight) && text1_y_value !== (text1_y_value = -1*ctx.titleHeight/8)) {
					setAttribute(text1, "y", text1_y_value);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(text1);
				}
			}
		};
	}

	// (50:6) {#if practice.goal}
	function create_if_block$3(component, ctx) {
		var text1, text0_value = ctx.practice.goal.label, text0, text1_id_value;

		return {
			c() {
				text1 = createSvgElement("text");
				text0 = createText(text0_value);
				setAttribute(text1, "dy", "-100");
				setAttribute(text1, "id", text1_id_value = ctx.practice.goal.id);
				setAttribute(text1, "font-size", ctx.goalFontSize);
				setAttribute(text1, "font-family", "Helvetica Neue, Helvetica, Arial, sans-serif");
			},

			m(target, anchor) {
				insert(target, text1, anchor);
				append(text1, text0);
			},

			p(changed, ctx) {
				if ((changed.disciplinesAnnotated) && text0_value !== (text0_value = ctx.practice.goal.label)) {
					setData(text0, text0_value);
				}

				if ((changed.disciplinesAnnotated) && text1_id_value !== (text1_id_value = ctx.practice.goal.id)) {
					setAttribute(text1, "id", text1_id_value);
				}

				if (changed.goalFontSize) {
					setAttribute(text1, "font-size", ctx.goalFontSize);
				}
			},

			d(detach) {
				if (detach) {
					detachNode(text1);
				}
			}
		};
	}

	// (43:4) {#each discipline.practices as practice}
	function create_each_block_1(component, ctx) {
		var text1, text0_value = ctx.practice.label, text0, text1_id_value, if_block_anchor;

		var if_block = (ctx.practice.goal) && create_if_block$3(component, ctx);

		var slice_initial_data = {
		 	centerX: ctx.centerX,
		 	centerY: ctx.centerY,
		 	startRadians: ctx.practice.startRadians,
		 	endRadians: ctx.practice.endRadians,
		 	innerRadius: ctx.innerRadius,
		 	outerRadius: ctx.outerRadius-ctx.disciplineBandHeight,
		 	levels: ctx.practice.levels,
		 	assessmentData: ctx.practice.assessmentData,
		 	label: ctx.practice.label,
		 	description: ctx.practice.description,
		 	goal: ctx.practice.goal,
		 	goalFontSize: ctx.goalFontSize,
		 	goalColor: ctx.goalColor,
		 	labelBandHeight: ctx.practiceBandHeight,
		 	stroke: ctx.practiceStroke,
		 	strokeWidth: ctx.strokeWidth,
		 	fontColor: ctx.fontColor,
		 	fontSize: ctx.practiceFontSize,
		 	levelConfigAnnotated: ctx.levelConfigAnnotated
		 };
		var slice = new Slice({
			root: component.root,
			store: component.store,
			data: slice_initial_data
		});

		slice.on("assessment-mouseover", function(event) {
			component.assessmentMouseover(event);
		});
		slice.on("assessment-mouseout", function(event) {
			component.assessmentMouseout(event);
		});

		return {
			c() {
				text1 = createSvgElement("text");
				text0 = createText(text0_value);
				if (if_block) if_block.c();
				if_block_anchor = createComment();
				slice._fragment.c();
				setAttribute(text1, "dy", "-100");
				setAttribute(text1, "id", text1_id_value = ctx.practice.id);
				setAttribute(text1, "font-size", ctx.practiceFontSize);
				setAttribute(text1, "font-family", "Helvetica Neue, Helvetica, Arial, sans-serif");
			},

			m(target, anchor) {
				insert(target, text1, anchor);
				append(text1, text0);
				if (if_block) if_block.m(target, anchor);
				insert(target, if_block_anchor, anchor);
				slice._mount(target, anchor);
			},

			p(changed, ctx) {
				if ((changed.disciplinesAnnotated) && text0_value !== (text0_value = ctx.practice.label)) {
					setData(text0, text0_value);
				}

				if ((changed.disciplinesAnnotated) && text1_id_value !== (text1_id_value = ctx.practice.id)) {
					setAttribute(text1, "id", text1_id_value);
				}

				if (changed.practiceFontSize) {
					setAttribute(text1, "font-size", ctx.practiceFontSize);
				}

				if (ctx.practice.goal) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block$3(component, ctx);
						if_block.c();
						if_block.m(if_block_anchor.parentNode, if_block_anchor);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}

				var slice_changes = {};
				if (changed.centerX) slice_changes.centerX = ctx.centerX;
				if (changed.centerY) slice_changes.centerY = ctx.centerY;
				if (changed.disciplinesAnnotated) slice_changes.startRadians = ctx.practice.startRadians;
				if (changed.disciplinesAnnotated) slice_changes.endRadians = ctx.practice.endRadians;
				if (changed.innerRadius) slice_changes.innerRadius = ctx.innerRadius;
				if (changed.outerRadius || changed.disciplineBandHeight) slice_changes.outerRadius = ctx.outerRadius-ctx.disciplineBandHeight;
				if (changed.disciplinesAnnotated) slice_changes.levels = ctx.practice.levels;
				if (changed.disciplinesAnnotated) slice_changes.assessmentData = ctx.practice.assessmentData;
				if (changed.disciplinesAnnotated) slice_changes.label = ctx.practice.label;
				if (changed.disciplinesAnnotated) slice_changes.description = ctx.practice.description;
				if (changed.disciplinesAnnotated) slice_changes.goal = ctx.practice.goal;
				if (changed.goalFontSize) slice_changes.goalFontSize = ctx.goalFontSize;
				if (changed.goalColor) slice_changes.goalColor = ctx.goalColor;
				if (changed.practiceBandHeight) slice_changes.labelBandHeight = ctx.practiceBandHeight;
				if (changed.practiceStroke) slice_changes.stroke = ctx.practiceStroke;
				if (changed.strokeWidth) slice_changes.strokeWidth = ctx.strokeWidth;
				if (changed.fontColor) slice_changes.fontColor = ctx.fontColor;
				if (changed.practiceFontSize) slice_changes.fontSize = ctx.practiceFontSize;
				if (changed.levelConfigAnnotated) slice_changes.levelConfigAnnotated = ctx.levelConfigAnnotated;
				slice._set(slice_changes);
			},

			d(detach) {
				if (detach) {
					detachNode(text1);
				}

				if (if_block) if_block.d(detach);
				if (detach) {
					detachNode(if_block_anchor);
				}

				slice.destroy(detach);
			}
		};
	}

	// (37:2) {#each disciplinesAnnotated as discipline}
	function create_each_block$2(component, ctx) {
		var text1, text0_value = ctx.discipline.label, text0, text1_id_value, each_anchor;

		var each_value_1 = ctx.discipline.practices;

		var each_blocks = [];

		for (var i = 0; i < each_value_1.length; i += 1) {
			each_blocks[i] = create_each_block_1(component, get_each_context_1(ctx, each_value_1, i));
		}

		var arc0_initial_data = {
		 	centerX: ctx.centerX,
		 	centerY: ctx.centerY,
		 	startRadians: ctx.discipline.startRadians,
		 	endRadians: ctx.discipline.endRadians,
		 	innerRadius: ctx.outerRadius-ctx.disciplineBandHeight,
		 	outerRadius: ctx.outerRadius,
		 	labelBandHeight: ctx.disciplineBandHeight,
		 	fill: "#FFFFFF",
		 	strokeWidth: ctx.strokeWidth * 2,
		 	stroke: ctx.disciplineStroke,
		 	label: ctx.discipline.label,
		 	description: ctx.discipline.description,
		 	fontColor: ctx.diciplineFontColor,
		 	fontSize: ctx.disciplineFontSize
		 };
		var arc0 = new Arc({
			root: component.root,
			store: component.store,
			data: arc0_initial_data
		});

		var arc1_initial_data = {
		 	centerX: ctx.centerX,
		 	centerY: ctx.centerY,
		 	startRadians: ctx.discipline.startRadians,
		 	endRadians: ctx.discipline.endRadians,
		 	outerRadius: ctx.outerRadius,
		 	innerRadius: ctx.innerRadius,
		 	fill: "none",
		 	strokeWidth: ctx.strokeWidth * 2,
		 	stroke: ctx.disciplineStroke
		 };
		var arc1 = new Arc({
			root: component.root,
			store: component.store,
			data: arc1_initial_data
		});

		return {
			c() {
				text1 = createSvgElement("text");
				text0 = createText(text0_value);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				each_anchor = createComment();
				arc0._fragment.c();
				arc1._fragment.c();
				setAttribute(text1, "dy", "-100");
				setAttribute(text1, "id", text1_id_value = ctx.discipline.id);
				setAttribute(text1, "font-size", ctx.disciplineFontSize);
				setAttribute(text1, "font-family", "Helvetica Neue, Helvetica, Arial, sans-serif");
			},

			m(target, anchor) {
				insert(target, text1, anchor);
				append(text1, text0);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(target, anchor);
				}

				insert(target, each_anchor, anchor);
				arc0._mount(target, anchor);
				arc1._mount(target, anchor);
			},

			p(changed, ctx) {
				if ((changed.disciplinesAnnotated) && text0_value !== (text0_value = ctx.discipline.label)) {
					setData(text0, text0_value);
				}

				if ((changed.disciplinesAnnotated) && text1_id_value !== (text1_id_value = ctx.discipline.id)) {
					setAttribute(text1, "id", text1_id_value);
				}

				if (changed.disciplineFontSize) {
					setAttribute(text1, "font-size", ctx.disciplineFontSize);
				}

				if (changed.centerX || changed.centerY || changed.disciplinesAnnotated || changed.innerRadius || changed.outerRadius || changed.disciplineBandHeight || changed.goalFontSize || changed.goalColor || changed.practiceBandHeight || changed.practiceStroke || changed.strokeWidth || changed.fontColor || changed.practiceFontSize || changed.levelConfigAnnotated) {
					each_value_1 = ctx.discipline.practices;

					for (var i = 0; i < each_value_1.length; i += 1) {
						const child_ctx = get_each_context_1(ctx, each_value_1, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_1(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(each_anchor.parentNode, each_anchor);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_1.length;
				}

				var arc0_changes = {};
				if (changed.centerX) arc0_changes.centerX = ctx.centerX;
				if (changed.centerY) arc0_changes.centerY = ctx.centerY;
				if (changed.disciplinesAnnotated) arc0_changes.startRadians = ctx.discipline.startRadians;
				if (changed.disciplinesAnnotated) arc0_changes.endRadians = ctx.discipline.endRadians;
				if (changed.outerRadius || changed.disciplineBandHeight) arc0_changes.innerRadius = ctx.outerRadius-ctx.disciplineBandHeight;
				if (changed.outerRadius) arc0_changes.outerRadius = ctx.outerRadius;
				if (changed.disciplineBandHeight) arc0_changes.labelBandHeight = ctx.disciplineBandHeight;
				if (changed.strokeWidth) arc0_changes.strokeWidth = ctx.strokeWidth * 2;
				if (changed.disciplineStroke) arc0_changes.stroke = ctx.disciplineStroke;
				if (changed.disciplinesAnnotated) arc0_changes.label = ctx.discipline.label;
				if (changed.disciplinesAnnotated) arc0_changes.description = ctx.discipline.description;
				if (changed.diciplineFontColor) arc0_changes.fontColor = ctx.diciplineFontColor;
				if (changed.disciplineFontSize) arc0_changes.fontSize = ctx.disciplineFontSize;
				arc0._set(arc0_changes);

				var arc1_changes = {};
				if (changed.centerX) arc1_changes.centerX = ctx.centerX;
				if (changed.centerY) arc1_changes.centerY = ctx.centerY;
				if (changed.disciplinesAnnotated) arc1_changes.startRadians = ctx.discipline.startRadians;
				if (changed.disciplinesAnnotated) arc1_changes.endRadians = ctx.discipline.endRadians;
				if (changed.outerRadius) arc1_changes.outerRadius = ctx.outerRadius;
				if (changed.innerRadius) arc1_changes.innerRadius = ctx.innerRadius;
				if (changed.strokeWidth) arc1_changes.strokeWidth = ctx.strokeWidth * 2;
				if (changed.disciplineStroke) arc1_changes.stroke = ctx.disciplineStroke;
				arc1._set(arc1_changes);
			},

			d(detach) {
				if (detach) {
					detachNode(text1);
				}

				destroyEach(each_blocks, detach);

				if (detach) {
					detachNode(each_anchor);
				}

				arc0.destroy(detach);
				arc1.destroy(detach);
			}
		};
	}

	function Radar(options) {
		init(this, options);
		this._state = assign(data$4(), options.data);

		this._recompute({ title: 1, showTitle: 1, titleHeight: 1, radarSize: 1, legendWidth: 1, disciplines: 1, practiceCount: 1, outerRadius: 1, disciplineBandHeight: 1, sliceWidth: 1, practiceBandHeight: 1, practiceMaxWidth: 1, practiceMaxHeight: 1, levelConfig: 1, baseColor: 1, descriptionsInLegend: 1, levelConfigAnnotated: 1, teams: 1, highlightedTeamID: 1, lookupLevelByID: 1, lookupTeamByID: 1, assessmentData: 1, innerRadius: 1, goalColor: 1 }, this._state);
		this._intro = true;

		this._fragment = create_main_fragment$4(this, this._state);

		this.root._oncreate.push(() => {
			oncreate.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(Radar.prototype, proto);
	assign(Radar.prototype, methods$1);

	Radar.prototype._recompute = function _recompute(changed, state) {
		if (changed.title || changed.showTitle || changed.titleHeight) {
			if (this._differs(state.viewBoxStartY, (state.viewBoxStartY = viewBoxStartY(state)))) changed.viewBoxStartY = true;
		}

		if (changed.radarSize || changed.title || changed.showTitle || changed.titleHeight) {
			if (this._differs(state.viewBoxHeight, (state.viewBoxHeight = viewBoxHeight(state)))) changed.viewBoxHeight = true;
		}

		if (changed.radarSize || changed.legendWidth) {
			if (this._differs(state.centerXViewBox, (state.centerXViewBox = centerXViewBox(state)))) changed.centerXViewBox = true;
		}

		if (changed.radarSize) {
			if (this._differs(state.centerX, (state.centerX = centerX(state)))) changed.centerX = true;
			if (this._differs(state.centerY, (state.centerY = centerY(state)))) changed.centerY = true;
			if (this._differs(state.outerRadius, (state.outerRadius = outerRadius(state)))) changed.outerRadius = true;
			if (this._differs(state.disciplineBandHeight, (state.disciplineBandHeight = disciplineBandHeight(state)))) changed.disciplineBandHeight = true;
			if (this._differs(state.practiceBandHeight, (state.practiceBandHeight = practiceBandHeight(state)))) changed.practiceBandHeight = true;
		}

		if (changed.disciplines) {
			if (this._differs(state.practiceCount, (state.practiceCount = practiceCount(state)))) changed.practiceCount = true;
		}

		if (changed.practiceCount) {
			if (this._differs(state.sliceWidth, (state.sliceWidth = sliceWidth(state)))) changed.sliceWidth = true;
		}

		if (changed.outerRadius || changed.disciplineBandHeight || changed.sliceWidth) {
			if (this._differs(state.disciplineMaxWidth, (state.disciplineMaxWidth = disciplineMaxWidth(state)))) changed.disciplineMaxWidth = true;
		}

		if (changed.outerRadius || changed.disciplineBandHeight || changed.practiceBandHeight || changed.sliceWidth) {
			if (this._differs(state.practiceMaxWidth, (state.practiceMaxWidth = practiceMaxWidth(state)))) changed.practiceMaxWidth = true;
		}

		if (changed.practiceMaxWidth) {
			if (this._differs(state.goalMaxWidth, (state.goalMaxWidth = goalMaxWidth(state)))) changed.goalMaxWidth = true;
		}

		if (changed.disciplineBandHeight) {
			if (this._differs(state.disciplineMaxHeight, (state.disciplineMaxHeight = disciplineMaxHeight(state)))) changed.disciplineMaxHeight = true;
		}

		if (changed.practiceBandHeight) {
			if (this._differs(state.practiceMaxHeight, (state.practiceMaxHeight = practiceMaxHeight(state)))) changed.practiceMaxHeight = true;
		}

		if (changed.practiceMaxHeight) {
			if (this._differs(state.goalMaxHeight, (state.goalMaxHeight = goalMaxHeight(state)))) changed.goalMaxHeight = true;
		}

		if (changed.levelConfig || changed.baseColor || changed.descriptionsInLegend) {
			if (this._differs(state.levelConfigAnnotated, (state.levelConfigAnnotated = levelConfigAnnotated(state)))) changed.levelConfigAnnotated = true;
		}

		if (changed.levelConfigAnnotated) {
			if (this._differs(state.lookupLevelByID, (state.lookupLevelByID = lookupLevelByID(state)))) changed.lookupLevelByID = true;
		}

		if (changed.teams) {
			if (this._differs(state.lookupTeamByID, (state.lookupTeamByID = lookupTeamByID(state)))) changed.lookupTeamByID = true;
		}

		if (changed.highlightedTeamID || changed.levelConfigAnnotated || changed.lookupLevelByID || changed.lookupTeamByID || changed.disciplines || changed.assessmentData || changed.sliceWidth || changed.innerRadius || changed.outerRadius || changed.practiceBandHeight || changed.disciplineBandHeight) {
			if (this._differs(state.disciplinesAnnotated, (state.disciplinesAnnotated = disciplinesAnnotated(state)))) changed.disciplinesAnnotated = true;
		}

		if (changed.goalColor || changed.levelConfigAnnotated) {
			if (this._differs(state.goalColor, (state.goalColor = goalColor(state)))) changed.goalColor = true;
		}
	};

	return Radar;

})));
//# sourceMappingURL=radar.js.map
